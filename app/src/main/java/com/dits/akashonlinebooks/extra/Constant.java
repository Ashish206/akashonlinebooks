package com.dits.akashonlinebooks.extra;


import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;


public class Constant {
    public static String stateName;
    public static String cityName;
    public static String landMark;
    public static String countryName;
    public static String postalCode;
    public static String uid;
    public static String userName;
    public static String userPhone;
    public static String userEmail;
    public static String userAddress;
    public static String userToken;
    public static String userLat;
    public static String userLng;
    public static DatabaseReference rootRef;
    public static DatabaseReference BooksDetailRef;
    public static DatabaseReference cartRef;
    public static DatabaseReference userRef;
    public static DatabaseReference requestBookRef,wishListRef,Referrals,offerZoneRef;
    public static String CartItemCount="0";
    public static String deliveryCharge="0";
    public static String totalPayableAmount="0";
    public static int paymentMethod = -1;
    public static DatabaseReference historyRef;
    public static int breaker= 0;
    public static int referralAmount = 0;
    public static int useableReferralAmount = 0;
    public static String mid="LrhqTt03485992271413";
    public static boolean isAddressChanged = false;
    //FXtwcL58662394023302
    //V!p1TRH!QST8sbvk
    private OnResponseFromServer onResponseFromServer;

    public static String BASE_URL = "https://akashpustaksadan.com/app/";

    public static boolean isNetworkConnected = true;

    public interface OnResponseFromServer{
        void onEvent(String response);
    }

    public static String upperCaseWords(String sentence) {
        String words[] = sentence.replaceAll("\\s+", " ").trim().split(" ");
        String newSentence = "";
        for (String word : words) {
            for (int i = 0; i < word.length(); i++)
                newSentence = newSentence + ((i == 0) ? word.substring(i, i + 1).toUpperCase():
                        (i != word.length() - 1) ? word.substring(i, i + 1).toLowerCase() : word.substring(i, i + 1).toLowerCase().toLowerCase() + " ");
        }

        return newSentence;
    }

    public String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    public void getDataFromServer(Context ctx, int type, String path, OnResponseFromServer eventListener, final HashMap hm){
        onResponseFromServer=eventListener;
        RequestQueue queue = Volley.newRequestQueue(ctx);
        StringRequest stringRequest = new StringRequest(type, Constant.BASE_URL + path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(onResponseFromServer!=null){
                        onResponseFromServer.onEvent(response);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    if(onResponseFromServer!=null){
                        onResponseFromServer.onEvent("error");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE:>>> ", error.toString() + "<<<");
                if(onResponseFromServer!=null){
                    onResponseFromServer.onEvent("error");
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return hm;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }
        };

        queue.add(stringRequest);

    }


}


