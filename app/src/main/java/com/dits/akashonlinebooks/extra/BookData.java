package com.dits.akashonlinebooks.extra;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;


import com.dits.akashonlinebooks.activity.HomeActivity;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

public class BookData {

    public static ArrayList<BookDataModel> bookData;
    public static void BookData(){
        bookData = new ArrayList<>();

        Constant.rootRef.child("BooksDetail").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                bookData.clear();
                for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                    BookDataModel bookDataModel = ds.getValue(BookDataModel.class);
                    bookData.add(bookDataModel);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
