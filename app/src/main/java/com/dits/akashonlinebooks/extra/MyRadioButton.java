package com.dits.akashonlinebooks.extra;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.dits.akashonlinebooks.R;


public class MyRadioButton implements View.OnClickListener{

    private TextView iv;
    private TextView tv;
    private RadioButton rb;
    private View view;

    public MyRadioButton(Context context) {

        view = View.inflate(context, R.layout.my_radio_button, null);
        rb =view.findViewById(R.id.radioButton1);
        tv =  view.findViewById(R.id.tv1);
        iv = view.findViewById(R.id.tv2);

        view.setOnClickListener(this);
        rb.setOnCheckedChangeListener(null);

    }


    public View getView() {
        return view;
    }

    @Override
    public void onClick(View v) {

        boolean nextState = !rb.isChecked();

        if (!rb.isChecked()){
            LinearLayout lGroup = (LinearLayout)view.getParent();
            if(lGroup != null){
                int child = lGroup.getChildCount();
                for(int i=0; i<child; i++){
                    //uncheck all
                    ((RadioButton)lGroup.getChildAt(i).findViewById(R.id.radioButton1)).setChecked(false);
                }
            }
            rb.setChecked(nextState);
            for (int i =0; i < lGroup.getChildCount(); i++){
                if (((RadioButton)lGroup.getChildAt(i).findViewById(R.id.radioButton1)).isChecked()){
                    Constant.paymentMethod = i;
                }
            }
        }

    }

    public void setSecondaryText(String b){
        iv.setText(b);
    }

    public void setText(String text){
        tv.setText(text);
    }

    public void setChecked(boolean isChecked){
        rb.setChecked(isChecked);
    }


}
