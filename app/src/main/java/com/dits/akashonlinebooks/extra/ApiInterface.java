package com.dits.akashonlinebooks.extra;

import com.dits.akashonlinebooks.model.AuthorModel;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.model.PublisherModel;
import com.dits.akashonlinebooks.model.SearchResultModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("bookDetails/forHome/")
    Call<List<BookDataModel>> getHomeBookDetails(@Query("filterBy") String filterBy);

    @GET("bookDetails/forHome/offerList/")
    Call<List<BookDataModel>> getOfferList(@Query("bookKeyList") String bookKeyList);

    @GET("bookDetails/offerList/")
    Call<List<BookDataModel>> getAllOfferList(@Query("bookKeyList") String bookKeyList);

    @GET("bookDetails/bookKey/")
    Call<List<BookDataModel>> getSingleBookDetails(@Query("bookKey") String bookKey);

    @GET("bookDetails/")
    Call<List<BookDataModel>> getAllBookDetails(@Query("page") int page);

    @GET("bookDetails/filterBy/")
    Call<List<BookDataModel>> getFilteredList(@Query("page") int page,@Query("filterKey") String filterKey,@Query("filterBy") String filterBy);

    @GET("bookDetails/search/")
    Call<List<SearchResultModel>> getSearchResult(@Query("searchKey") String searchKey);

    @GET("bookDetails/searchResult/")
    Call<List<BookDataModel>> getSearchDetailsResult(@Query("searchKey") String searchKey);

    @GET("bookDetails/authorsList/")
    Call<List<AuthorModel>> getAuthorList();

    @GET("bookDetails/publisherList/")
    Call<List<PublisherModel>> getPublisherList();


    @GET("bookDetails/AuthorFilter/")
    Call<List<BookDataModel>> getBookByAuthor(@Query("filterBy") String filterBy,@Query("bookKey") String bookKey);

    @GET("bookDetails/CategoryFilter/")
    Call<List<BookDataModel>> getBookByCategory(@Query("filterBy") String filterBy,@Query("bookKey") String bookKey);

    @GET("deductQuantity/")
    Call<String> deductQuantity(@Query("bookKey") String bookKey);

}
