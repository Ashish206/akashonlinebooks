package com.dits.akashonlinebooks.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.activity.HomeActivity;
import com.dits.akashonlinebooks.extra.ApiClient;
import com.dits.akashonlinebooks.extra.ApiInterface;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.AuthorModel;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.model.PublisherModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterFragment extends DialogFragment {

    ListView categoryList,subCatList;
    String catArray[] ={"Category","Author","Publisher"};
    DatabaseReference mainRef;
    List<String> categoryArray,authorArray,publisherArray;
    ArrayAdapter adapter;
    ImageView backBtn;
    TextView clearFilterBtn;
    RelativeLayout progressBar;
    EditText inputSearch;
    ClearFilterListener clearFilterListener;
    SetFilter setFilter;
    String filterKey="Category",filterBy;
    private ApiInterface movieService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_filter, container, false);
        categoryList = v.findViewById(R.id.View01);
        subCatList = v.findViewById(R.id.View02);


        clearFilterListener = BookListFragment.ctx;
        setFilter = BookListFragment.ctx;

        mainRef = Constant.rootRef.child("Study");

        categoryArray = new ArrayList<>();
        authorArray = new ArrayList<>();
        publisherArray = new ArrayList<>();
        inputSearch = v.findViewById(R.id.search_bar);

        movieService = ApiClient.getClient().create(ApiInterface.class);

        final ArrayAdapter catAdapter = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,catArray);
        categoryList.setAdapter(catAdapter);
        loadCategory();

        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0){
                    filterKey = "Category";
                    inputSearch.setHint("Search Category");
                    adapter = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,categoryArray);
                    subCatList.setAdapter(adapter);
                }else if (i==1){
                    filterKey = "Author";
                    inputSearch.setHint("Search Author");
                    adapter = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,authorArray);
                    subCatList.setAdapter(adapter);
                }else if (i == 2){
                    filterKey = "Publisher";
                    inputSearch.setHint("Search Publisher");
                    adapter = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,publisherArray);
                    subCatList.setAdapter(adapter);
                }
            }
        });



        progressBar = v.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        backBtn = v.findViewById(R.id.backBtn);
        clearFilterBtn = v.findViewById(R.id.clear_filter_btn);
        clearFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clearFilterListener != null) {
                    clearFilterListener.clearFilterListener();
                }
                dismiss();
            }
        });


        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });




        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        subCatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                filterBy = ((TextView) view ).getText().toString();
                if (setFilter != null) {
                    setFilter.setFilter(filterKey,filterBy);
                }
                dismiss();
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Window window = getDialog().getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    private void loadCategory(){
        mainRef.child("Category").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    categoryArray.add(Character.toUpperCase(ds.child("category").getValue().toString().charAt(0)) + ds.child("category").getValue().toString().substring(1).toLowerCase());
                }
                adapter = new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,categoryArray);
                subCatList.setAdapter(adapter);
                loadAuthor();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadAuthor(){
        getAuthorList().enqueue(new Callback<List<AuthorModel>>() {
            @Override
            public void onResponse(Call<List<AuthorModel>> call, Response<List<AuthorModel>> response) {
                for (AuthorModel ds : response.body()){
                    authorArray.add(Character.toUpperCase(ds.getAuthor().charAt(0)) + ds.getAuthor().substring(1).toLowerCase());
                }
                loadPublisher();
            }

            @Override
            public void onFailure(Call<List<AuthorModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private void loadPublisher(){
        getPublisher().enqueue(new Callback<List<PublisherModel>>() {
            @Override
            public void onResponse(Call<List<PublisherModel>> call, Response<List<PublisherModel>> response) {
                for (PublisherModel ds : response.body()){
                    publisherArray.add(Character.toUpperCase(ds.getPublisher().charAt(0)) + ds.getPublisher().substring(1).toLowerCase());
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<PublisherModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme);
    }

    private Call<List<AuthorModel>> getAuthorList() {
        return movieService.getAuthorList();
    }

    private Call<List<PublisherModel>> getPublisher() {
        return movieService.getPublisherList();
    }

    public interface ClearFilterListener {
        void clearFilterListener();
    }

    public interface SetFilter {
        void setFilter(String filterKey,String filterBy);
    }
}
