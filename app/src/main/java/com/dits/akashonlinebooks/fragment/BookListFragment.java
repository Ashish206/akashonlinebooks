package com.dits.akashonlinebooks.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.activity.BookDetailActivity;
import com.dits.akashonlinebooks.activity.HomeActivity;
import com.dits.akashonlinebooks.activity.MoreBtnActivity;
import com.dits.akashonlinebooks.adapter.BookDetailGridAdapter;
import com.dits.akashonlinebooks.extra.ApiClient;
import com.dits.akashonlinebooks.extra.ApiInterface;
import com.dits.akashonlinebooks.extra.BookData;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.utils.PaginationAdapterCallback;
import com.dits.akashonlinebooks.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookListFragment extends Fragment implements PaginationAdapterCallback,BookDetailGridAdapter.ItemListener
        ,FilterFragment.ClearFilterListener,FilterFragment.SetFilter,HomeActivity.ClearFilterListenerHome, BookDetailGridAdapter.ShowNoItemText{

    public static BookDetailGridAdapter adapter;
    GridLayoutManager manager;
    RecyclerView rv;
    public static ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    TextView txtError;
    SwipeRefreshLayout swipeRefreshLayout;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    public static boolean isLastPage = false;
    private static final int TOTAL_PAGES = 100;
    private int currentPage = PAGE_START;
    private ApiInterface movieService;
    public static BookListFragment ctx;
    private TextView no_item_text;
    View v;
    RelativeLayout filterBtn,sortBtn;
    public static boolean isSearchResult = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_book_list, container, false);

        rv = v.findViewById(R.id.bookList);
        progressBar = v.findViewById(R.id.main_progress);
        errorLayout = v.findViewById(R.id.error_layout);
        btnRetry = v.findViewById(R.id.error_btn_retry);
        txtError = v.findViewById(R.id.error_txt_cause);
        swipeRefreshLayout = v.findViewById(R.id.main_swiperefresh);
        no_item_text = v.findViewById(R.id.no_item_text);
        no_item_text.setVisibility(View.GONE);
        filterBtn = v.findViewById(R.id.filter_btn);
        sortBtn = v.findViewById(R.id.sort_btn);

        ctx = BookListFragment.this;

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                FilterFragment filterFragment = new FilterFragment();
                filterFragment.show(ft, "dialog");
            }
        });



        rv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return false;
            }
        });

        adapter = new BookDetailGridAdapter(getContext(),BookListFragment.this,BookListFragment.this);

        manager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);
        rv.setLayoutManager(manager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);

        movieService = ApiClient.getClient().create(ApiInterface.class);

        loadFirstPage();

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFirstPage();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isLastPage){
                    doRefresh();
                }else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                //start your activity here
                HomeActivity.searchView.requestFocus();
            }

        }, 500);

        sortBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(getContext(), sortBtn);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.filter_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.one){
                            adapter.sortLowToHigh();
                        }else if (item.getItemId() == R.id.two){
                            adapter.sortHignToLow();
                        }
                        return true;
                    }
                });

                if (isSearchResult){
                    popup.show();//showing popup menu
                }else {
                    Toast.makeText(getContext(), "Not Available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        swipeRefreshLayout.requestFocus();
    }

    public interface FilterClick{
        void onFilterClick();
    }

    private void doRefresh() {
        progressBar.setVisibility(View.VISIBLE);
        if (callTopRatedMoviesApi().isExecuted())
            callTopRatedMoviesApi().cancel();

        // TODO: Check if data is stale.
        //  Execute network request if cache is expired; otherwise do not update data.
        adapter.clear();
        adapter.notifyDataSetChanged();
        loadFirstPage();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void loadFirstPage() {
        isSearchResult = false;
        rv.addOnScrollListener(new PaginationScrollListener(manager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();
        currentPage = PAGE_START;

        callTopRatedMoviesApi().enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                hideErrorView();


//                SearchResultFragment searchResultFragment = new SearchResultFragment();
//                searchResultFragment.show(HomeActivity.fragmentManager, searchResultFragment.getTag());

                progressBar.setVisibility(View.GONE);
                adapter.addAll(response.body());
                BookListFragment.adapter.checkListCount();
                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    public void loadFirstPage(String filterKey,String filterBy) {
        isSearchResult = true;
        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();
//        currentPage = PAGE_START;

        filterBy(filterKey,filterBy).enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                hideErrorView();

                isLastPage = true;

                progressBar.setVisibility(View.GONE);
                adapter.addAll(response.body());

//                if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
//                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }


    private void loadNextPage() {


        callTopRatedMoviesApi().enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
//                Log.i(TAG, "onResponse: " + currentPage
//                        + (response.raw().cacheResponse() != null ? "Cache" : "Network"));

                adapter.removeLoadingFooter();
                isLoading = false;

//                List<BookDetailsModel> results = fetchResults(response);
                adapter.addAll(response.body());

                if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                adapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    /**
     * Performs a Retrofit call to the top rated movies API.
     * Same API call for Pagination.
     * As {@link #currentPage} will be incremented automatically
     * by @{@link PaginationScrollListener} to load next page.
     */
    private Call<List<BookDataModel>> callTopRatedMoviesApi() {
        return movieService.getAllBookDetails(
                currentPage
        );
    }

    private Call<List<BookDataModel>> filterBy(String filterKey,String filterBy) {
        return movieService.getFilteredList(
                currentPage,
                filterKey,
                filterBy
        );
    }

    @Override
    public void retryPageLoad() {
        loadNextPage();
    }

    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent intent = new Intent(getContext(), BookDetailActivity.class);
        intent.putExtra("key",key);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                getActivity(), imageView, "image_transition");
        startActivity(intent);
    }

    @Override
    public void clearFilterListener(){
        adapter.clear();
        loadFirstPage();

    }
    @Override
    public void setFilter(String filterKey,String filterBy){
        adapter.clear();
        swipeRefreshLayout.requestFocus();
        progressBar.setVisibility(View.VISIBLE);
        loadFirstPage(filterKey,filterBy);
    }


    @Override
    public void ClearFilterListenerHome() {
        isSearchResult = false;
        adapter.clear();
        loadFirstPage();
    }

    @Override
    public void showNoItemText(boolean b) {
        if (b){
            no_item_text.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }else {
            no_item_text.setVisibility(View.GONE);
        }
    }
}
