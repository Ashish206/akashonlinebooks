package com.dits.akashonlinebooks.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.activity.MoreBtnActivity;
import com.dits.akashonlinebooks.adapter.HomeRecyclerViewAdapter;
import com.dits.akashonlinebooks.extra.BookData;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.HomeRVModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeDynamicFragment extends Fragment implements HomeRecyclerViewAdapter.MoreBtnListener,
        HomeRecyclerViewAdapter.BannerClick {

    ArrayList slidersUrl;
    ShimmerRecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    ArrayList<HomeRVModel> homeArrayList;
    CardView whatsapp_card;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v  = inflater.inflate(R.layout.fragment_home_dynamic, container, false);
        recyclerView = v.findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.showShimmerAdapter();
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        whatsapp_card = v.findViewById(R.id.whatsapp_card);

        whatsapp_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWhatsApp("+917440905555");
            }
        });

        slidersUrl = new ArrayList();
        homeArrayList = new ArrayList<>();
        loadMainView();
        return v;
    }

    private void openWhatsApp(String number) {
        try {
            number = number.replace(" ", "").replace("+", "");

            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number)+"@s.whatsapp.net");
            // getApplication().startActivity(sendIntent);

            startActivity(Intent.createChooser(sendIntent, "Compartir en")
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

        } catch(Exception e) {
            Log.e("WS", "ERROR_OPEN_MESSANGER"+e.toString());
        }
    }

    private void loadMainView(){

        Constant.rootRef.child("HomeScreen").orderByChild("position").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                    String type = ds.child("type").getValue().toString();

                    if (type.equals("horizontal_list")){

                        HomeRVModel homeRVModel = new HomeRVModel();
                        homeRVModel.setCategory_name(ds.child("category_name").getValue().toString());
                        homeRVModel.setFilter(ds.child("filter").getValue().toString());
                        homeRVModel.setType(ds.child("type").getValue().toString());
                        homeRVModel.setSlider1("null");
                        homeRVModel.setSlider2("null");
                        homeRVModel.setSlider3("null");
                        homeRVModel.setImage("null");

                        homeArrayList.add(homeRVModel);


                    }else if(type.equals("slider")){

                        HomeRVModel homeRVModel = new HomeRVModel();
                        homeRVModel.setCategory_name(ds.child("category_name").getValue().toString());
                        homeRVModel.setFilter("null");
                        homeRVModel.setType(ds.child("type").getValue().toString());
                        homeRVModel.setSlider1(ds.child("slider1").getValue().toString());
                        homeRVModel.setSlider2(ds.child("slider2").getValue().toString());
                        homeRVModel.setSlider3(ds.child("slider3").getValue().toString());
                        homeRVModel.setSlider1_click(ds.child("slider1_click").getValue().toString());
                        homeRVModel.setSlider2_click(ds.child("slider2_click").getValue().toString());
                        homeRVModel.setSlider3_click(ds.child("slider3_click").getValue().toString());
                        homeRVModel.setBanner_click("null");
                        homeRVModel.setImage("null");

                        homeArrayList.add(homeRVModel);


                    }else if (type.equals("banner")){

                        HomeRVModel homeRVModel = new HomeRVModel();
                        homeRVModel.setCategory_name(ds.child("category_name").getValue().toString());
                        homeRVModel.setFilter("null");
                        homeRVModel.setType(ds.child("type").getValue().toString());
                        homeRVModel.setSlider1("null");
                        homeRVModel.setSlider2("null");
                        homeRVModel.setSlider3("null");

                        homeRVModel.setSlider1_click("null");
                        homeRVModel.setSlider2_click("null");
                        homeRVModel.setSlider3_click("null");
                        homeRVModel.setBanner_click(ds.child("banner_click").getValue().toString());

                        homeRVModel.setImage(ds.child("image").getValue().toString());

                        homeArrayList.add(homeRVModel);

                    }else if (type.equals("offer_zone")){
                        HomeRVModel homeRVModel = new HomeRVModel();
                        homeRVModel.setCategory_name(ds.child("category_name").getValue().toString());
                        homeRVModel.setFilter(ds.child("filter").getValue().toString());
                        homeRVModel.setType(ds.child("type").getValue().toString());
                        homeRVModel.setSlider1("null");
                        homeRVModel.setSlider2("null");
                        homeRVModel.setSlider3("null");
                        homeRVModel.setSlider1_click("null");
                        homeRVModel.setSlider2_click("null");
                        homeRVModel.setSlider3_click("null");
                        homeRVModel.setBanner_click("null");
                        homeRVModel.setImage("null");
                        homeArrayList.add(homeRVModel);
                    }
                }
                //HomeBaseAdapter homeBaseAdapter = new HomeBaseAdapter(homeArrayList,getContext(),getActivity(),HomeDynamicFragment.this);
                HomeRecyclerViewAdapter homeRecyclerViewAdapter = new HomeRecyclerViewAdapter(homeArrayList,getContext(),getActivity(),HomeDynamicFragment.this,HomeDynamicFragment.this);
                recyclerView.setAdapter(homeRecyclerViewAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onMoreBtnClick(String filterBy,String heading) {
        Intent i = new Intent(getContext(), MoreBtnActivity.class);
        i.putExtra("category",filterBy);
        i.putExtra("heading",heading);
        startActivity(i);
    }


    @Override
    public void onBannerClick(String filterBy,String heading) {
        if (!filterBy.equals("null")){
            Intent i = new Intent(getContext(), MoreBtnActivity.class);
            i.putExtra("category",filterBy);
            i.putExtra("heading",heading);
            startActivity(i);
        }

    }
}
