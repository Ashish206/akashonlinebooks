package com.dits.akashonlinebooks.model;

public class OfferModel {
    String category,photo;

    public OfferModel() {
    }

    public OfferModel(String category, String photo) {
        this.category = category;
        this.photo = photo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}

