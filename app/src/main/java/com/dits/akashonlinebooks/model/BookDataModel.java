package com.dits.akashonlinebooks.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookDataModel {

    @SerializedName("AboutAuthor")
    @Expose
    private String aboutAuthor;
    @SerializedName("actualCost")
    @Expose
    private String actualCost;
    @SerializedName("AdditionalBookCode")
    @Expose
    private String additionalBookCode;
    @SerializedName("Author")
    @Expose
    private String author;
    @SerializedName("Binding")
    @Expose
    private String binding;
    @SerializedName("BookAbout")
    @Expose
    private String bookAbout;
    @SerializedName("BookName")
    @Expose
    private String bookName;
    @SerializedName("BookTags")
    @Expose
    private String bookTags;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("Descriptions")
    @Expose
    private String descriptions;
    @SerializedName("Dimensions")
    @Expose
    private String dimensions;
    @SerializedName("Edition")
    @Expose
    private String edition;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("isbn")
    @Expose
    private String isbn;
    @SerializedName("bookKey")
    @Expose
    private String bookKey;
    @SerializedName("MediumType")
    @Expose
    private String mediumType;
    @SerializedName("MRP")
    @Expose
    private String mRP;
    @SerializedName("NumberOfPages")
    @Expose
    private Integer numberOfPages;
    @SerializedName("Publisher")
    @Expose
    private String publisher;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("Ranking")
    @Expose
    private String ranking;
    @SerializedName("Rating")
    @Expose
    private String rating;
    @SerializedName("SaleCurrency")
    @Expose
    private String saleCurrency;
    @SerializedName("Weight")
    @Expose
    private String weight;

    private int bookQuantity;

    public int getBookQuantity() {
        return bookQuantity;
    }

    public void setBookQuantity(int bookQuantity) {
        this.bookQuantity = bookQuantity;
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public BookDataModel() {
    }

    /**
     *
     * @param mRP
     * @param edition
     * @param weight
     * @param saleCurrency
     * @param bookKey
     * @param image
     * @param mediumType
     * @param actualCost
     * @param descriptions
     * @param publisher
     * @param aboutAuthor
     * @param author
     * @param category
     * @param additionalBookCode
     * @param isbn
     * @param binding
     * @param dimensions
     * @param quantity
     * @param numberOfPages
     * @param rating
     * @param bookAbout
     * @param bookTags
     * @param ranking
     * @param bookName
     */
    public BookDataModel(String aboutAuthor, String actualCost, String additionalBookCode, String author, String binding, String bookAbout, String bookName, String bookTags, String category, String descriptions, String dimensions, String edition, String image, String isbn, String bookKey, String mediumType, String mRP, Integer numberOfPages, String publisher, Integer quantity, String ranking, String rating, String saleCurrency, String weight) {
        super();
        this.aboutAuthor = aboutAuthor;
        this.actualCost = actualCost;
        this.additionalBookCode = additionalBookCode;
        this.author = author;
        this.binding = binding;
        this.bookAbout = bookAbout;
        this.bookName = bookName;
        this.bookTags = bookTags;
        this.category = category;
        this.descriptions = descriptions;
        this.dimensions = dimensions;
        this.edition = edition;
        this.image = image;
        this.isbn = isbn;
        this.bookKey = bookKey;
        this.mediumType = mediumType;
        this.mRP = mRP;
        this.numberOfPages = numberOfPages;
        this.publisher = publisher;
        this.quantity = quantity;
        this.ranking = ranking;
        this.rating = rating;
        this.saleCurrency = saleCurrency;
        this.weight = weight;
    }

    public String getAboutAuthor() {
        return aboutAuthor;
    }

    public void setAboutAuthor(String aboutAuthor) {
        this.aboutAuthor = aboutAuthor;
    }

    public String getActualCost() {
        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }

    public String getAdditionalBookCode() {
        return additionalBookCode;
    }

    public void setAdditionalBookCode(String additionalBookCode) {
        this.additionalBookCode = additionalBookCode;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBinding() {
        return binding;
    }

    public void setBinding(String binding) {
        this.binding = binding;
    }

    public String getBookAbout() {
        return bookAbout;
    }

    public void setBookAbout(String bookAbout) {
        this.bookAbout = bookAbout;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookTags() {
        return bookTags;
    }

    public void setBookTags(String bookTags) {
        this.bookTags = bookTags;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getBookKey() {
        return bookKey;
    }

    public void setBookKey(String bookKey) {
        this.bookKey = bookKey;
    }

    public String getMediumType() {
        return mediumType;
    }

    public void setMediumType(String mediumType) {
        this.mediumType = mediumType;
    }

    public String getMRP() {
        return mRP;
    }

    public void setMRP(String mRP) {
        this.mRP = mRP;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSaleCurrency() {
        return saleCurrency;
    }

    public void setSaleCurrency(String saleCurrency) {
        this.saleCurrency = saleCurrency;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

}
