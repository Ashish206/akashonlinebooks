package com.dits.akashonlinebooks.model;

public class ReviewModel {
    String Date,name,review,uid;

    public ReviewModel() {
    }

    public ReviewModel(String date, String name, String review, String uid) {
        Date = date;
        this.name = name;
        this.review = review;
        this.uid = uid;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
