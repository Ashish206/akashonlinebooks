package com.dits.akashonlinebooks.model;

public class HistoryListModel {
    String ActualTotalAmount,TotalAmount,date,delivered,deliveryBoyName,deliveryBoyNumber,deliveryCharge,order_id,paymentMethod,time,userAddress,
            userId,userLat,userLng,userName,userPhone,userPostalCode,orderIdUser,referralAmount;

    public HistoryListModel() {
    }

    public HistoryListModel(String actualTotalAmount, String totalAmount, String date, String delivered, String deliveryBoyName, String deliveryBoyNumber, String deliveryCharge, String order_id, String paymentMethod, String time, String userAddress, String userId, String userLat, String userLng, String userName, String userPhone, String userPostalCode, String orderIdUser, String referralAmount) {
        ActualTotalAmount = actualTotalAmount;
        TotalAmount = totalAmount;
        this.date = date;
        this.delivered = delivered;
        this.deliveryBoyName = deliveryBoyName;
        this.deliveryBoyNumber = deliveryBoyNumber;
        this.deliveryCharge = deliveryCharge;
        this.order_id = order_id;
        this.paymentMethod = paymentMethod;
        this.time = time;
        this.userAddress = userAddress;
        this.userId = userId;
        this.userLat = userLat;
        this.userLng = userLng;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userPostalCode = userPostalCode;
        this.orderIdUser = orderIdUser;
        this.referralAmount = referralAmount;
    }

    public String getActualTotalAmount() {
        return ActualTotalAmount;
    }

    public void setActualTotalAmount(String actualTotalAmount) {
        ActualTotalAmount = actualTotalAmount;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getDeliveryBoyName() {
        return deliveryBoyName;
    }

    public void setDeliveryBoyName(String deliveryBoyName) {
        this.deliveryBoyName = deliveryBoyName;
    }

    public String getDeliveryBoyNumber() {
        return deliveryBoyNumber;
    }

    public void setDeliveryBoyNumber(String deliveryBoyNumber) {
        this.deliveryBoyNumber = deliveryBoyNumber;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserLat() {
        return userLat;
    }

    public void setUserLat(String userLat) {
        this.userLat = userLat;
    }

    public String getUserLng() {
        return userLng;
    }

    public void setUserLng(String userLng) {
        this.userLng = userLng;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserPostalCode() {
        return userPostalCode;
    }

    public void setUserPostalCode(String userPostalCode) {
        this.userPostalCode = userPostalCode;
    }

    public String getOrderIdUser() {
        return orderIdUser;
    }

    public void setOrderIdUser(String orderIdUser) {
        this.orderIdUser = orderIdUser;
    }

    public String getReferralAmount() {
        return referralAmount;
    }

    public void setReferralAmount(String referralAmount) {
        this.referralAmount = referralAmount;
    }
}
