package com.dits.akashonlinebooks.model;

public class QnAModel {
    String name,question,answer,uid,question_id;

    public QnAModel() {
    }

    public QnAModel(String name, String question, String answer, String uid, String question_id) {
        this.name = name;
        this.question = question;
        this.answer = answer;
        this.uid = uid;
        this.question_id = question_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }
}
