package com.dits.akashonlinebooks.model;

public class HomeRVModel {
    public HomeRVModel() {
    }

    String category_name,filter,type,slider1,slider2,slider3,image,slider1_click,slider2_click,slider3_click,banner_click;

    public HomeRVModel(String category_name, String filter, String type, String slider1, String slider2, String slider3, String image, String slider1_click, String slider2_click, String slider3_click, String banner_click) {
        this.category_name = category_name;
        this.filter = filter;
        this.type = type;
        this.slider1 = slider1;
        this.slider2 = slider2;
        this.slider3 = slider3;
        this.image = image;
        this.slider1_click = slider1_click;
        this.slider2_click = slider2_click;
        this.slider3_click = slider3_click;
        this.banner_click = banner_click;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlider1() {
        return slider1;
    }

    public void setSlider1(String slider1) {
        this.slider1 = slider1;
    }

    public String getSlider2() {
        return slider2;
    }

    public void setSlider2(String slider2) {
        this.slider2 = slider2;
    }

    public String getSlider3() {
        return slider3;
    }

    public void setSlider3(String slider3) {
        this.slider3 = slider3;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSlider1_click() {
        return slider1_click;
    }

    public void setSlider1_click(String slider1_click) {
        this.slider1_click = slider1_click;
    }

    public String getSlider2_click() {
        return slider2_click;
    }

    public void setSlider2_click(String slider2_click) {
        this.slider2_click = slider2_click;
    }

    public String getSlider3_click() {
        return slider3_click;
    }

    public void setSlider3_click(String slider3_click) {
        this.slider3_click = slider3_click;
    }

    public String getBanner_click() {
        return banner_click;
    }

    public void setBanner_click(String banner_click) {
        this.banner_click = banner_click;
    }
}
