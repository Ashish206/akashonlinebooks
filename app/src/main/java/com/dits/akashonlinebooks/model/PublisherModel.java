package com.dits.akashonlinebooks.model;

public class PublisherModel {
    String Publisher;

    public PublisherModel() {
    }

    public PublisherModel(String publisher) {
        Publisher = publisher;
    }

    public String getPublisher() {
        return Publisher;
    }

    public void setPublisher(String publisher) {
        Publisher = publisher;
    }
}
