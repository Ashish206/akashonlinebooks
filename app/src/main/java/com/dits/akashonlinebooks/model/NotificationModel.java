package com.dits.akashonlinebooks.model;

public class NotificationModel {
    String title,message,date,image_url;

    public NotificationModel() {
    }

    public NotificationModel(String title, String message, String date, String image_url) {
        this.title = title;
        this.message = message;
        this.date = date;
        this.image_url = image_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
