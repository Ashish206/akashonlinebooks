package com.dits.akashonlinebooks.model;

public class SearchResultModel {
    String searchResult;

    public SearchResultModel() {
    }

    public SearchResultModel(String bookName) {
        this.searchResult = bookName;
    }

    public String getBookName() {
        return searchResult;
    }

    public void setBookName(String bookName) {
        this.searchResult = bookName;
    }
}
