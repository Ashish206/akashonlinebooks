package com.dits.akashonlinebooks.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.extra.TinyDB;
import com.dits.akashonlinebooks.model.NotificationModel;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;

public class FcmTokenRefreshService extends FirebaseMessagingService {
    public FcmTokenRefreshService() {
    }
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        try{
            sp = PreferenceManager.getDefaultSharedPreferences(this);
            editor = sp.edit();
            Constant.userToken = s;
            editor.putString("tokenId",s);
            editor.apply();
            Constant.userRef.child("tokenId").setValue(s);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        TinyDB tinydb = new TinyDB(getApplicationContext());
        ArrayList<Object> notificationData = new ArrayList();
        notificationData = tinydb.getListObject("notificationData",NotificationModel.class);

        NotificationModel nm = new NotificationModel();
        nm.setTitle(remoteMessage.getData().get("title"));
        nm.setMessage(remoteMessage.getData().get("message"));
        nm.setDate(remoteMessage.getData().get("date"));
        nm.setImage_url(remoteMessage.getData().get("image_url"));
        notificationData.add(nm);
        tinydb.putListObject("notificationData", notificationData);

    }
}
