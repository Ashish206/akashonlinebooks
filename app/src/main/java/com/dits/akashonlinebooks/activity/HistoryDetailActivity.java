package com.dits.akashonlinebooks.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.adapter.HistoryDetailAdapter;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.HistoryDetailModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class HistoryDetailActivity extends AppCompatActivity implements HistoryDetailAdapter.CancelOrder,HistoryDetailAdapter.Rating,HistoryDetailAdapter.DownloadInvoice{

    ArrayList<HistoryDetailModel> bookDetailList;
    public static String Cost,ActualCost,date,delivered,orderKey,orderref,paymentMethod,userAddress,userId,userName,userPhone,userPostalCode,deliveryCharge,referralAmount;

    RecyclerView history_detail;
    LinearLayoutManager linearLayoutManager;
    public static int count = 0;
    ProgressDialog progressDialog;
    float avg;
    WebView invoiceWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);

        Cost = getIntent().getExtras().getString("Cost");
        ActualCost = getIntent().getExtras().getString("ActualCost");
        date = getIntent().getExtras().getString("date");
        delivered = getIntent().getExtras().getString("delivered");
        orderKey = getIntent().getExtras().getString("orderKey");
        orderref = getIntent().getExtras().getString("orderref");
        paymentMethod = getIntent().getExtras().getString("paymentMethod");
        userAddress = getIntent().getExtras().getString("userAddress");
        userId = getIntent().getExtras().getString("userId");
        userName = getIntent().getExtras().getString("userName");
        userPhone = getIntent().getExtras().getString("userPhone");
        userPostalCode = getIntent().getExtras().getString("userPostalCode");
        deliveryCharge = getIntent().getExtras().getString("deliveryCharge");
        referralAmount = getIntent().getExtras().getString("referralAmount");
        history_detail = findViewById(R.id.history_detail);
        history_detail.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        linearLayoutManager = new LinearLayoutManager(this);
        history_detail.setLayoutManager(linearLayoutManager);
        history_detail.setHasFixedSize(true);
        bookDetailList = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        invoiceWebView = findViewById(R.id.myWebView22);
        invoiceWebView.loadUrl(Constant.BASE_URL + "getInvoice/" + orderKey);
        invoiceWebView.getSettings().setJavaScriptEnabled(true);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Order Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        invoiceWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageCommitVisible(WebView view, String url) {
            }
        });

        loadHistoryDetails();
    }


    @Override
    protected void onResume() {
        super.onResume();
        count = 0;
    }

    @Override
    protected void onStart() {
        super.onStart();
        count = 0;
    }

    void loadHistoryDetails(){
        Constant.historyRef.child(orderref).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                bookDetailList.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    if (ds.hasChild("product_id")){
                        HistoryDetailModel historyDetailModel = new HistoryDetailModel();
                        historyDetailModel.setActualAmount(ds.child("ActualAmount").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("Amount").getValue().toString());
                        historyDetailModel.setAuthor(ds.child("Author").getValue().toString());
                        historyDetailModel.setBookName(ds.child("BookName").getValue().toString());
                        historyDetailModel.setImage(ds.child("Image").getValue().toString());
                        historyDetailModel.setMRP(ds.child("MRP").getValue().toString());
                        historyDetailModel.setPublisher(ds.child("Publisher").getValue().toString());
                        historyDetailModel.setKey(ds.child("key").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("actualCost").getValue().toString());
                        historyDetailModel.setQuantity(ds.child("quantity").getValue().toString());
                        historyDetailModel.setActualCost(ds.child("actualCost").getValue().toString());
                        historyDetailModel.setRated(ds.child("Rated").getValue().toString());
                        historyDetailModel.setReviewed(ds.child("reviewed").getValue().toString());
                        historyDetailModel.setHistoryKey(ds.getKey());
                        bookDetailList.add(historyDetailModel);
                        count++;
                    }
                }

                HistoryDetailAdapter historyDetailAdapter = new HistoryDetailAdapter(getApplicationContext(),bookDetailList,HistoryDetailActivity.this);
                history_detail.setAdapter(historyDetailAdapter);
                historyDetailAdapter.setData(bookDetailList);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onClacelOrderClick(final String key) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HistoryDetailActivity.this);
        builder.setMessage("Do you really want to cancel this order?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Constant.historyRef.child(HistoryDetailActivity.orderref).child("delivered").setValue("Cancelled");
                        FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders").
                                child(key).child("delivered").setValue("Cancelled");
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRateingListener(final String key, final float rate,float oldRating,String historyKey) {

        float f = 0;
        progressDialog.show();
        if (rate!=0.0){
            f= oldRating+rate;
            if (oldRating == 0){
                avg = f;
            }else {
                avg=f/2;
            }
            Constant.historyRef.child(HistoryDetailActivity.orderref).child(historyKey).child("Rated").setValue("true").addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Constant.BooksDetailRef.child(key).child("Rating").setValue(String.valueOf(new DecimalFormat("#.#").format(avg))).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            progressDialog.dismiss();
                            loadHistoryDetails();
                        }
                    });
                }
            });

        }else {
            progressDialog.dismiss();
        }

    }

    @Override
    public void onDownloadInvoiceClick() {
        createWebPrintJob(invoiceWebView);
    }

    private void createWebPrintJob(WebView webView) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            PrintManager printManager = (PrintManager) this
                    .getSystemService(Context.PRINT_SERVICE);

            PrintDocumentAdapter printAdapter =
                    null;
            printAdapter = webView.createPrintDocumentAdapter("Invoice");
            String jobName = getString(R.string.app_name) + " Print Test";

            printManager.print(jobName, printAdapter,
                    new PrintAttributes.Builder().build());
        } else {
            Toast.makeText(HistoryDetailActivity.this, "Print job has been canceled! ", Toast.LENGTH_SHORT).show();
        }

    }
}
