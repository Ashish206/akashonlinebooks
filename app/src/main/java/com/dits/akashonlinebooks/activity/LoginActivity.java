package com.dits.akashonlinebooks.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.extra.Constant;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends AppCompatActivity {


    private static final int RC_SIGN_IN = 123;
    private RelativeLayout rootLayout;
    DatabaseReference userRef;
    ProgressDialog progressDialog;
    TextInputEditText emailField,nameField,referralField;
    public static TextInputEditText addressField;
    public static String email,name,address,city;
    Button signupBtn;

    protected static final int REQUEST_CHECK_SETTINGS = 2;
    public static LatLng latLng = null;
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    public static String addressString = "";
    public static String cityString = "";
    public static String stateString = "";
    public static String countryString = "";
    public static String postalCodeString = "";

   boolean isReferred = false;
   String referredBy = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        rootLayout = findViewById(R.id.rootLayout);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        latLng = new LatLng(0.0, 0.0);
        isNetworkConnectionAvailable();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        userRef = FirebaseDatabase.getInstance().getReference().child("UserInfo");
        nameField = findViewById(R.id.nameField);
        addressField = findViewById(R.id.addressField);
        signupBtn = findViewById(R.id.signupBtn);
        emailField = findViewById(R.id.emailField);
        referralField = findViewById(R.id.referralField);
        try{
            referredBy =getIntent().getExtras().getString("referrerUid","null");
        }catch (Exception e){

        }
        if (!referredBy.equals("null")){
            referralField.setVisibility(View.VISIBLE);
            referralField.setText(referredBy);
            referralField.setEnabled(false);
            isReferred=true;
        }else {
            referralField.setVisibility(View.GONE);
        }
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidEmail(emailField.getText().toString())){
                    if (TextUtils.isEmpty(emailField.getText().toString())){
                        emailField.setError("enter email");
                        emailField.requestFocus();
                    }
                    else if (TextUtils.isEmpty(nameField.getText().toString())){
                        nameField.setError("enter name");
                        nameField.requestFocus();
                    }else {
                        progressDialog.show();
                        email = emailField.getText().toString().trim();
                        editor.apply();
                        name = nameField.getText().toString().trim();
                        //address = addressField.getText().toString().trim();
                        city = "Jabalpur";
                        attemptUserLoginViaFirebase();
                    }
                }else {
                    emailField.setError("Enter Valid Email");
                    emailField.requestFocus();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void attemptUserLoginViaFirebase()  {

        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.PhoneBuilder().build());
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }


    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS){
            if ( resultCode == RESULT_OK){
                startActivity(new Intent(LoginActivity.this,GetAddressMapActivity.class));
            }
        }

        if (requestCode == RC_SIGN_IN){
            if (resultCode == RESULT_OK) {
                getTokenId();
            } else {
                progressDialog.dismiss();
                Snackbar
                        .make(rootLayout, "Firebase authentication failed", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void getTokenId() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull final Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("------>", "getInstanceId failed", task.getException());
                            return;
                        }else {

                            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            userRef.child(city).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    String token = task.getResult().getToken();
                                    progressDialog.show();
                                    HashMap hm = new HashMap();
                                    hm.put("email",email);
                                    hm.put("name",name);
                                    hm.put("phone",user.getPhoneNumber());
                                    hm.put("tokenId",token);
                                    if (!dataSnapshot.hasChild("referred_by") && isReferred){
                                        hm.put("referred_by",referredBy);
                                        hm.put("referralAmount","20");
                                        userRef.child(city).child(referredBy).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                String refAmount = dataSnapshot.child("referralAmount").getValue().toString();
                                                userRef.child(city).child(referredBy).child("referralAmount").setValue(String.valueOf(Integer.valueOf(refAmount)+20));
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                                    }else if (!dataSnapshot.hasChild("referred_by")) {
                                        hm.put("referred_by",referredBy);
                                        hm.put("referralAmount","0");
                                    }

                                    editor.putString("email",email);
                                    editor.putString("userName",name);
                                    editor.putString("phone",user.getPhoneNumber());
                                    editor.putString("tokenId",token);
                                    editor.apply();
                                    userRef.child(city).child(user.getUid()).updateChildren(hm)
                                            .addOnCompleteListener(new OnCompleteListener() {
                                                @Override
                                                public void onComplete(@NonNull Task task) {
                                                    if (task.isSuccessful()){
                                                        progressDialog.dismiss();
                                                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                                        finish();
                                                    }
                                                    else {
                                                        Toast.makeText(LoginActivity.this, "Opps!! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                        progressDialog.dismiss();
                                                    }
                                                }
                                            });
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });



                        }


                    }
                });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(permissions.length == 0){
            return;
        }
        boolean allPermissionsGranted = true;
        if(grantResults.length>0){
            for(int grantResult: grantResults){
                if(grantResult != PackageManager.PERMISSION_GRANTED){
                    allPermissionsGranted = false;
                    break;
                }
            }
        }
        if(!allPermissionsGranted){
            boolean somePermissionsForeverDenied = false;
            for(String permission: permissions){
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                    //denied
                    Log.e("denied", permission);
                }else{
                    if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                        //allowed
                        Log.e("allowed", permission);
                    } else{
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        somePermissionsForeverDenied = true;
                    }
                }
            }
            if(somePermissionsForeverDenied){
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("Permissions Required")
                        .setMessage("You have forcefully denied some of the required permissions " +
                                "for this action. Please open settings, go to permissions and allow them.")
                        .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", getPackageName(), null));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            }
        } else {
            switch (requestCode) {
                //act according to the request code used while requesting the permission(s).
            }
        }
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startActivity(new Intent(LoginActivity.this,GetAddressMapActivity.class));
                        //Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(LoginActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            //Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });

    }

    public void checkNetworkConnection() {
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("No internet Connection");
        builder.setMessage("Please turn on internet connection to continue");
        builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean isNetworkConnectionAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if(isConnected) {
            Log.d("Network", "Connected");
            return true;
        }
        else{
            checkNetworkConnection();
            Log.d("Network","Not Connected");
            return false;
        }
    }

}

