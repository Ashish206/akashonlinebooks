package com.dits.akashonlinebooks.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.adapter.HomeRecyclerViewAdapter;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.HomeRVModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AddDataTOFirebase extends AppCompatActivity {
    DatabaseReference mRef;
    ArrayList authList;
    List authListwithoutDuplicate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data_tofirebase);

        authList = new ArrayList();
        authListwithoutDuplicate = new ArrayList();

        //findCategory();
       // getAuthorList();
    }

    private void getAuthorList(){
        FirebaseDatabase.getInstance().getReference().child("BooksDetail").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                    authList.add(ds.child("Author").getValue().toString());
                }
                authListwithoutDuplicate = clearListFromDuplicateFirstName(authList);

                for(int i=0;i<authListwithoutDuplicate.size();i++){
                    Log.d("---->",""+authListwithoutDuplicate.get(i));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private List clearListFromDuplicateFirstName(List list1) {

        Map<String, Object> cleanMap = new LinkedHashMap<String, Object>();
        for (int i = 0; i < list1.size(); i++) {
            cleanMap.put((String) list1.get(i), list1.get(i));
        }
        List list = new ArrayList<>(cleanMap.values());
        return list;
    }


    private void findCategory(){
        FirebaseDatabase.getInstance().getReference().child("Study").child("Category").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    for (int i = 0; i<getResources().getStringArray(R.array.category).length;i++){
                        if (ds.child("category").getValue().toString().toLowerCase().equals(getResources().getStringArray(R.array.category)[i].toLowerCase())){
                        }
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getData() {
        String url = "http://61.246.140.151/bookregistration/";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseData(response);
            }
        },
        new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE:>>> ", error.toString() + "<<<");
            }
        });

        queue.add(request);
    }

    private void parseData(String response) {
        JSONObject jsonObjetct;
        try {
            JSONArray jsonArray = new JSONArray(response);

            for(int i=0; i<220; i++){
                jsonObjetct = jsonArray.getJSONObject(i);
                HashMap hm = new HashMap();
                if (!jsonObjetct.getString("Image").equals("nill")){
                    hm.put("isbn",jsonObjetct.getString("isbn"));
                    hm.put("Author",jsonObjetct.getString("Author"));
                    hm.put("BookName",jsonObjetct.getString("BookName"));
                    hm.put("Publisher",jsonObjetct.getString("Publisher"));
                    hm.put("MRP",jsonObjetct.getString("MRP"));
                    hm.put("Discount",jsonObjetct.getString("Discount"));
                    hm.put("Description",jsonObjetct.getString("Description"));
                    hm.put("Image",jsonObjetct.getString("Image"));
                    hm.put("Category",jsonObjetct.getString("Category"));
                    hm.put("Medium",jsonObjetct.getString("Medium"));
                    hm.put("NumberOfPages",jsonObjetct.getString("NumberOfPages"));
                    hm.put("Edition",jsonObjetct.getString("Edition"));
                    hm.put("BookTags",jsonObjetct.getString("BookTags"));
                    hm.put("BookAbout",jsonObjetct.getString("BookAbout"));
                    hm.put("AboutAuthor",jsonObjetct.getString("AboutAuthor"));
                    hm.put("Binding",jsonObjetct.getString("Binding"));
                    hm.put("Dimensions",jsonObjetct.getString("Dimensions"));
                    hm.put("Weight",jsonObjetct.getString("Weight"));
                    hm.put("AdditionalBookCode",jsonObjetct.getString("AdditionalBookCode"));
                    hm.put("SaleCurrency",jsonObjetct.getString("SaleCurrency"));

                    hm.put("Quantity",jsonObjetct.getString("Quantity"));
                    hm.put("Rating","5");
                    hm.put("Ranking","0");

                    uploadDataToFirebase(hm);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void uploadDataToFirebase(HashMap hm) {
        String key = mRef.push().getKey();
        mRef.child(key).updateChildren(hm);

    }

//    public static void deleteBook(){
//        FirebaseDatabase.getInstance().getReference().child("BooksDetail").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for (final DataSnapshot ds : dataSnapshot.getChildren()) {
//                    if (ds.child("isbn").getValue().toString().contains("12345")){
//                        FirebaseDatabase.getInstance().getReference().child("BooksDetail").child(ds.getKey()).setValue(null);
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }

//    public static void addKey() {
//        FirebaseDatabase.getInstance().getReference().child("BooksDetail").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for (final DataSnapshot ds : dataSnapshot.getChildren()) {
//                    FirebaseDatabase.getInstance().getReference().child("BooksDetail").child(ds.getKey()).child("Rating").setValue("0");
//                    FirebaseDatabase.getInstance().getReference().child("BooksDetail").child(ds.getKey()).child("Ranking").setValue("0");
////                    Constant.rootRef.child("BooksDetail").child(ds.getKey()).child("key").setValue(ds.getKey());
////                    Constant.rootRef.child("BooksDetail").child(ds.getKey()).child("actualCost").setValue("200");
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
}
