package com.dits.akashonlinebooks.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.extra.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.HashMap;

public class ReferralActivity extends AppCompatActivity {
    public final static int WHITE = 0xFFFFFFFF;
    public final static int BLACK = 0xFF000000;
    public final static int WIDTH = 400;
    public final static int HEIGHT = 400;

    Button shareBtn;
    ImageView qrCodeScanBtn;

    int dbRefAmount = 20;
    private IntentIntegrator qrScan;
    ProgressDialog progressDialog;
    TextView refAmountTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");


        shareBtn = findViewById(R.id.share_and_earn_btn);
        qrCodeScanBtn = findViewById(R.id.qr_code_btn);
        qrScan = new IntentIntegrator(this);
        refAmountTxt = findViewById(R.id.refAmountTxt);
        refAmountTxt.setText("Wallet Amount : ₹"+Constant.referralAmount);

        qrScan.setOrientationLocked(false);
        qrCodeScanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrScan.initiateScan();
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareLink();
            }
        });

        ImageView imageView = findViewById(R.id.qr_code_img);
        try {
            Bitmap bitmap = encodeAsBitmap(Constant.uid);
            imageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }


    public Uri createDynamicLink_Basic() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String uid = user.getUid();
        String link = "https://akashonlinebooks/?invitedby=" + uid;
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(link))
                .setDomainUriPrefix("https://akashonlinebooks.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.akashonlinebooks.ios").build())
                .buildDynamicLink();
        return dynamicLink.getUri();
    }

    public void shareLink() {
        Intent sendIntent = new Intent();
        String msg = "Download the app with this referral and get ₹20 on your wallet " + createDynamicLink_Basic();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void shareReferralUrl() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        String subject = "Check out Akash Online Books";
        share.putExtra(Intent.EXTRA_SUBJECT, subject);

        share.putExtra(Intent.EXTRA_TEXT, "Do you want to buy books? Just download this amazing app and get your item at your door step. " +"https://play.google.com/store/apps/details?id=" + getPackageName());

        startActivity(Intent.createChooser(share, "Share link!"));
    }


    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, WIDTH, HEIGHT, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }

        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        return bitmap;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                if (result.getContents()!=null && !result.getContents().equals(Constant.uid)){
                    progressDialog.show();
                    processQrCode(result.getContents());
                }else {
                    Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                }
                //Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void processQrCode(final String uid) {
        final int[] x = {0};

        Constant.Referrals.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                x[0] = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren())
                {
                    if (ds.child("uid").getValue().toString().equals(uid)){
                        x[0] = 1;
                        break;
                    }
                }
                if (x[0] != 1){
                    useCode(uid);
                }else {
                    progressDialog.dismiss();
                    showAlreadDialog();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void useCode(String uid) {
        String key = Constant.Referrals.push().getKey();
        HashMap hm = new HashMap();
        hm.put("uid",uid);
        Constant.Referrals.child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                Constant.referralAmount  = dbRefAmount + Constant.referralAmount;
                Constant.userRef.child("referralAmount").setValue(String.valueOf(Integer.valueOf(Constant.referralAmount))).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        refAmountTxt.setText("Wallet Amount : ₹"+Constant.referralAmount);
                        Toast.makeText(ReferralActivity.this, "You have got ₹"+dbRefAmount, Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
            }
        });
    }

    private void showAlreadDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage("You have already used this qr code");
        alertDialog.show();
    }

}
