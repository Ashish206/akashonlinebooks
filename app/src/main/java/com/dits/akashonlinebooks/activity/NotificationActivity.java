package com.dits.akashonlinebooks.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.LinearLayout;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.adapter.NotificationAdapter;
import com.dits.akashonlinebooks.extra.TinyDB;
import com.dits.akashonlinebooks.model.NotificationModel;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity implements NotificationAdapter.OnDelete {
    RecyclerView history_list;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog progressDialog;
    LinearLayout noItemLayout;
    NotificationAdapter notificationAdapter;
    TinyDB tinydb;
    ArrayList<Object> notificationData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        noItemLayout = findViewById(R.id.noItemLayout);
        noItemLayout.setVisibility(View.GONE);

        history_list = findViewById(R.id.history_list);
        history_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        history_list.setLayoutManager(linearLayoutManager);
        history_list.setHasFixedSize(true);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
//        progressDialog.show();

        notificationData = new ArrayList<>();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Notifications");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        tinydb = new TinyDB(getApplicationContext());
        notificationData = tinydb.getListObject("notificationData", NotificationModel.class);
        notificationAdapter = new NotificationAdapter(notificationData,NotificationActivity.this);
        history_list.setAdapter(notificationAdapter);

        if (notificationData.size()==0){
            noItemLayout.setVisibility(View.VISIBLE);
        }else {
            noItemLayout.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onDelete(int i) {
        notificationData.remove(i);
        tinydb.putListObject("notificationData", notificationData);
        notificationAdapter = new NotificationAdapter(notificationData,NotificationActivity.this);
        history_list.setAdapter(notificationAdapter);
        if (notificationData.size()==0){
            noItemLayout.setVisibility(View.VISIBLE);
        }else {
            noItemLayout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        notificationAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
