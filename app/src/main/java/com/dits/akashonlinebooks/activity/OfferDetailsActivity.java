package com.dits.akashonlinebooks.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.adapter.BookDetailGridAdapter;
import com.dits.akashonlinebooks.extra.BookData;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.extra.CountDrawable;
import com.dits.akashonlinebooks.fragment.BookListFragment;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.model.OfferIdModer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;



public class OfferDetailsActivity extends AppCompatActivity implements BookDetailGridAdapter.ItemListener{
    SearchView searchView;
    TextView heading;
    RecyclerView recyclerView;

    ArrayList<BookDataModel> bookList;
    ImageView cartBtn;
    static CountDrawable badge;
    ArrayList<OfferIdModer> idList;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);

        bookList = new ArrayList<>();

        idList = new ArrayList();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        searchView = findViewById(R.id.search_bar);
        heading = findViewById(R.id.titleText);
        heading.setText("Offer Zone");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.recycler_view);
       // bookDetailGridAdapter = new BookDetailGridAdapter(getApplicationContext(), bookList,this);
       // recyclerView.setAdapter(bookDetailGridAdapter);
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        cartBtn = findViewById(R.id.cart_btn);
        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OfferDetailsActivity.this,CartActivity.class));
            }
        });

        LayerDrawable icon = (LayerDrawable) cartBtn.getDrawable();

        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(this);
        }

        badge.setCount(HomeActivity.getCartItemCount());
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                //BookListFragment.bookDetailGridAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ///BookListFragment.bookDetailGridAdapter.getFilter().filter(s);
                return true;
            }
        });
        loadDetails();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void loadDetails() {
        bookList.clear();
        idList.clear();
        Constant.rootRef.child("OfferZoneBooks").child(getIntent().getExtras().getString("id")).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    idList.add(ds.getValue(OfferIdModer.class));
                }
                getBooks();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getBooks() {
        int lp = 0;
        while (lp < idList.size()){
            for (int i = 0; i<BookData.bookData.size(); i++){
                if (BookData.bookData.get(i).getBookKey().equals(idList.get(lp).getId())){
                    bookList.add(BookData.bookData.get(i));
                    break;
                }
            }
            lp++;
        }
        progressDialog.dismiss();
       // bookDetailGridAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent intent = new Intent(this, BookDetailActivity.class);
        intent.putExtra("key",key);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                OfferDetailsActivity.this, imageView, "image_transition");
        startActivity(intent, options.toBundle());
    }
}
