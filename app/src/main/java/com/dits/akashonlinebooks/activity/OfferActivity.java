package com.dits.akashonlinebooks.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.model.OfferModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import static com.dits.akashonlinebooks.extra.Constant.offerZoneRef;

public class OfferActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    RecyclerView itemList;
    TextView notDataText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        notDataText = findViewById(R.id.not_data_text);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Offer Zone");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        itemList = findViewById(R.id.itemList);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(new GridLayoutManager(this, 1));
        loadCategoryList();
    }


    private void loadCategoryList() {
        FirebaseRecyclerOptions<OfferModel> options =
                new FirebaseRecyclerOptions.Builder<OfferModel>()
                        .setQuery(offerZoneRef, OfferModel.class)
                        .build();

        loadList(options);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void loadList(FirebaseRecyclerOptions<OfferModel> options) {
        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<OfferModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_offer_zone_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(ItemViewHolder holder, int position, final OfferModel model) {
                holder.setItemName(model.getCategory());
                holder.setItemImage(model.getPhoto(),getApplicationContext());
                final String id =getRef(position).getKey();

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(OfferActivity.this, OfferDetailsActivity.class);
                        i.putExtra("id",id);
                        startActivity(i);
                    }
                });


            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (progressDialog != null && progressDialog.isShowing()) {
                    if (getItemCount()!=0){
                        notDataText.setVisibility(View.GONE);
                    }else {
                        notDataText.setVisibility(View.VISIBLE);
                    }
                    progressDialog.dismiss();
                }

            }
        };
        adapter.startListening();
        itemList.setAdapter(adapter);
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.textView);
            book_name.setText(item);

        }

        public void setItemImage(String item, Context context) {
            ImageView itemImage = mView.findViewById(R.id.categoryImage);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
        }

    }
}
