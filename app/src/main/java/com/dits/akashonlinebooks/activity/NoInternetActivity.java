package com.dits.akashonlinebooks.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.extra.Constant;

public class NoInternetActivity extends AppCompatActivity {

    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);

        button = findViewById(R.id.shop_now_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constant.isNetworkConnected){
                    finish();
                }else {
                    Toast.makeText(NoInternetActivity.this, "Turn on your internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {

        if (Constant.isNetworkConnected){
            super.onBackPressed();
        }else {
            Toast.makeText(NoInternetActivity.this, "Turn on your internet connection.", Toast.LENGTH_SHORT).show();
        }
    }
}
