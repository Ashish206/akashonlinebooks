package com.dits.akashonlinebooks.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.transition.Fade;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abdulhakeem.seemoretextview.SeeMoreTextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.adapter.HomeHorizontalRecyclerAdapter;
import com.dits.akashonlinebooks.adapter.HomeRecyclerViewAdapter;
import com.dits.akashonlinebooks.extra.ApiClient;
import com.dits.akashonlinebooks.extra.ApiInterface;
import com.dits.akashonlinebooks.extra.BookData;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.extra.CountDrawable;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.model.QnAModel;
import com.dits.akashonlinebooks.model.ReviewModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDetailActivity extends AppCompatActivity implements HomeHorizontalRecyclerAdapter.ItemListener{

    DatabaseReference detailRef;
    String rating;
    TextView bookNameTxt,AuthNAmeTxt,costTxt,actualCost,dicsountPercentage,ratingTxt,deliveryTxt,quantityStatus;
    ImageView imageView;
    SeeMoreTextView disTxt;
    FloatingActionButton back_btn;
    Button addToCart,buyNow;
    ImageView cartBtn;
    static CountDrawable badge;
    TextView keyLang,keyBinding,keyPublication,keyGenre,keyISBM,keyEdition,keyPages;
    ProgressDialog progressDialog;
    TextView shareTxt,wistlist_btn;
    String bookKey;
    boolean isInWishList = false;
    String wistlistKey;
    RecyclerView reviewList,qnaList;
    DatabaseReference reviewRef,qnaRef;
    TextView noQnaTxt,noRwvTxt,askQnaBtn;
    ProgressBar revProgress,qnaProgress;
    Context ctx;
    ArrayList<BookDataModel> bookDetailList,categoryList,authorList;
    ShimmerRecyclerView similarCategoryList,similarAuthorList;
    ApiInterface apiService;
    String category,author;
    RelativeLayout bottomBtn;
    int breaker = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        ctx = this;

        breaker = 0;
        bookKey = getIntent().getExtras().getString("key");
        detailRef = Constant.rootRef.child("BooksDetail");
        bookNameTxt = findViewById(R.id.book_name);
        AuthNAmeTxt = findViewById(R.id.author_name);
        disTxt = findViewById(R.id.discription);
        costTxt = findViewById(R.id.cost_text);
        actualCost = findViewById(R.id.actual_cost_text);
        imageView = findViewById(R.id.product_image);
        back_btn = findViewById(R.id.back_btn);
        addToCart = findViewById(R.id.addCartBtn);
        dicsountPercentage = findViewById(R.id.discount);
        buyNow =findViewById(R.id.buy_now);
        ratingTxt = findViewById(R.id.ratingTxt);
        quantityStatus = findViewById(R.id.special_price);

        deliveryTxt = findViewById(R.id.free_delivery_txt);

        noRwvTxt = findViewById(R.id.no_rev_txt);
        noQnaTxt = findViewById(R.id.no_qna_txt);
        revProgress = findViewById(R.id.rev_progress_bar);
        qnaProgress = findViewById(R.id.qna_progress_bar);

        keyLang = findViewById(R.id.key_lang);
        keyBinding = findViewById(R.id.key_binding);
        keyPublication = findViewById(R.id.key_publisher);
        keyGenre = findViewById(R.id.key_genre);
        keyISBM = findViewById(R.id.key_isbn);
        keyEdition = findViewById(R.id.key_edition);
        keyPages = findViewById(R.id.key_pages);
        cartBtn = findViewById(R.id.cart_btn);
        shareTxt = findViewById(R.id.share_btn);
        askQnaBtn = findViewById(R.id.ask_qna_btn);
        qnaList = findViewById(R.id.qna_list);
        bottomBtn = findViewById(R.id.bottomBtn);
        similarAuthorList = findViewById(R.id.similar_book_list_author);
        similarCategoryList = findViewById(R.id.similar_book_list_category);
        similarAuthorList.setHasFixedSize(true);
        similarAuthorList.setNestedScrollingEnabled(false);

        similarCategoryList.setHasFixedSize(true);
        similarCategoryList.setNestedScrollingEnabled(false);

        wistlist_btn = findViewById(R.id.wistlist_btn);


        reviewList = findViewById(R.id.review_list);
        reviewList.setLayoutManager(new LinearLayoutManager(this));
        qnaList.setLayoutManager(new LinearLayoutManager(this));

        reviewRef = FirebaseDatabase.getInstance().getReference().child("Review").child(getIntent().getExtras().getString("key"));
        qnaRef = FirebaseDatabase.getInstance().getReference().child("QnA").child(getIntent().getExtras().getString("key"));
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LayerDrawable icon = (LayerDrawable) cartBtn.getDrawable();

        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(this);
        }

        badge.setCount(HomeActivity.getCartItemCount());
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);

        countCartItem();
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                HashMap hm = new HashMap();
                hm.put("itemUid",getIntent().getExtras().getString("key"));
                hm.put("quantity","1");

                Constant.cartRef.child(getIntent().getExtras().getString("key")).updateChildren(hm).addOnSuccessListener(new OnSuccessListener() {
                    @Override
                    public void onSuccess(Object o) {
                        badge.setCount(HomeActivity.getCartItemCount());
                        progressDialog.dismiss();
                        Toast.makeText(BookDetailActivity.this, "Added in your cart", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                HashMap hm = new HashMap();
                hm.put("itemUid",getIntent().getExtras().getString("key"));
                hm.put("quantity","1");
                Constant.cartRef.child(getIntent().getExtras().getString("key")).updateChildren(hm).addOnSuccessListener(new OnSuccessListener() {
                    @Override
                    public void onSuccess(Object o) {
                        progressDialog.dismiss();
                        startActivity(new Intent(BookDetailActivity.this,CartActivity.class));

                    }
                });
            }
        });


        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BookDetailActivity.this,CartActivity.class));
            }
        });

        askQnaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showQuestionDialog();
            }
        });



        getWistList();

        bookDetailList = new ArrayList<>();
        categoryList = new ArrayList<>();
        authorList = new ArrayList<>();

        apiService =
                ApiClient.getClient().create(ApiInterface.class);



        detailRef.child(getIntent().getExtras().getString("key")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("------>",String.valueOf(dataSnapshot));

                rating = String.valueOf(Float.parseFloat(dataSnapshot.child("Rating").getValue().toString()));

                if (rating.equals("0.0") || rating.equals("0")){
                    ratingTxt.setText("No Ratings");
                }else {
                    ratingTxt.setText(rating);
                }

                if (!Constant.deliveryCharge.equals("0")){
                    deliveryTxt.setText("Delivery Charge : ₹"+Constant.deliveryCharge);
                }else {
                    deliveryTxt.setText("");
                    deliveryTxt.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        wistlist_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInWishList){
                    isInWishList = false;
                    wistlist_btn.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.wistlist_gray),null,null,null);
                    Constant.wishListRef.child(wistlistKey).setValue(null);
                    Toast.makeText(BookDetailActivity.this, "Removed from wishlist!!", Toast.LENGTH_SHORT).show();
                    getWistList();
                }else {
                    String key = Constant.wishListRef.push().getKey();
                    Constant.wishListRef.child(key).child("key").setValue(bookKey);
                    Toast.makeText(BookDetailActivity.this, "Added to wishlist!!", Toast.LENGTH_SHORT).show();
                    wistlist_btn.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.wishlist_red),null,null,null);
                    isInWishList = true;
                    getWistList();
                }
            }
        });


    }

    private void setupMainData(){
        if (!progressDialog.isShowing()){
            progressDialog.show();
        }
        bookDetailList.clear();
        Call<List<BookDataModel>> call = apiService.getSingleBookDetails(bookKey);
        call.enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {

                for (BookDataModel result : response.body()) {
                    bookDetailList.add(result);
                }


                keyLang.setText("Language: "+Constant.upperCaseWords(bookDetailList.get(0).getMediumType()));
                keyBinding.setText("Binding: "+bookDetailList.get(0).getBinding());
                keyPublication.setText("Publisher: "+Constant.upperCaseWords(bookDetailList.get(0).getPublisher()));
                keyGenre.setText("Category: "+Constant.upperCaseWords(bookDetailList.get(0).getCategory()));
                keyISBM.setText("ISBN: "+bookDetailList.get(0).getIsbn());
                keyEdition.setText("Edition: "+Constant.upperCaseWords(bookDetailList.get(0).getEdition()));
                keyPages.setText("Pages: "+bookDetailList.get(0).getNumberOfPages());
                int quantity = Integer.valueOf(bookDetailList.get(0).getQuantity());
                if (quantity>3){
                    quantityStatus.setText("Available");
                }else if (quantity<=0){
                    quantityStatus.setText("Out Of Stock");
                    quantityStatus.setTextColor(getResources().getColor(R.color.red));
                    bottomBtn.setVisibility(View.GONE);
                }else {
                    quantityStatus.setText("Hurry Up!! Only few books available");
                }


//                rating = String.valueOf(Float.parseFloat(dataSnapshot.child("Rating").getValue().toString()));

                category = bookDetailList.get(0).getCategory();
                author = bookDetailList.get(0).getAuthor();

                bookNameTxt.setText(Constant.upperCaseWords(bookDetailList.get(0).getBookName()));
                AuthNAmeTxt.setText(Constant.upperCaseWords(bookDetailList.get(0).getAuthor()));
                disTxt.setContent(bookDetailList.get(0).getDescriptions());
                disTxt.setTextMaxLength(300);
                disTxt.setSeeMoreTextColor(R.color.colorPrimary);
                disTxt.setNestedScrollingEnabled(false);
                disTxt.setScrollContainer(false);
                costTxt.setText("₹"+bookDetailList.get(0).getMRP());
                actualCost.setText("₹"+bookDetailList.get(0).getActualCost());
                dicsountPercentage.setText(setPersentageText(Float.parseFloat(bookDetailList.get(0).getMRP()),Float.parseFloat(bookDetailList.get(0).getActualCost()))+"% off");
                Glide.with(getApplicationContext()).load(bookDetailList.get(0).getImage()).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(imageView);

                shareTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        shareLink();
//                        Intent share = new Intent(android.content.Intent.ACTION_SEND);
//                        share.setType("text/plain");
//                        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//                        String subject = bookDetailList.get(0).getBookName();
//                        share.putExtra(Intent.EXTRA_SUBJECT, subject);
//                        share.putExtra(Intent.EXTRA_TEXT, "Buy this amazing book from Akash Online Books and get your book at your door step. " +"https://play.google.com/store/apps/details?id=" + getPackageName());
                        //startActivity(Intent.createChooser(share, "Share link!"));
                    }
                });

                if (breaker==0){
                    getSimilarData();
                    breaker++;
                }

                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

            }
        });

    }

    public Uri createDynamicLink_Basic() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String uid = user.getUid();
        String link = "https://akashonlinebooks/?bookid=" + bookKey;
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(link))
                .setDomainUriPrefix("https://akashonlinebooks.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.akashonlinebooks.ios").build())
                .buildDynamicLink();
        return dynamicLink.getUri();
    }

    public void shareLink() {
        Intent sendIntent = new Intent();
        String msg = "Buy this amazing book from Akash Online Books and get your book at your door step.  " + createDynamicLink_Basic();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void showQuestionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Post Question");
        builder.setMessage("write your question here.");
        LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View v=li.inflate(R.layout.ask_question_dialog_layout,null,false);
        builder.setView(v);
        final EditText feedback = v.findViewById(R.id.question_edit_text);
        feedback.setHint("write your question here");
        builder.setPositiveButton("Post Here", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!TextUtils.isEmpty(feedback.getText().toString().trim())){
                    DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
                    final String date = dateFormat.format(Calendar.getInstance().getTime());
                    String xbxb = qnaRef.push().getKey();

                    HashMap hm = new HashMap();
                    hm.put("name",Constant.userName);
                    hm.put("uid",Constant.uid);
                    hm.put("question",feedback.getText().toString().trim());
                    hm.put("answer","null");
                    hm.put("question_id",xbxb);
                    hm.put("Date",date);
                    qnaRef.child(xbxb).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                Toast.makeText(BookDetailActivity.this, "Posted...", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else {
                    Toast.makeText(BookDetailActivity.this, "Write somthing...", Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog ad=builder.create();
        ad.show();
    }

    private void getWistList(){
        Constant.wishListRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    if (d.child("key").getValue().toString().equals(bookKey)){
                        wistlistKey = d.getKey();
                        wistlist_btn.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.wishlist_red),null,null,null);
                        isInWishList = true;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadReviews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupMainData();
        loadReviews();
        loadQna();
    }

    private void loadQna() {
        FirebaseRecyclerOptions<QnAModel> options =
                new FirebaseRecyclerOptions.Builder<QnAModel>()
                        .setQuery(qnaRef, QnAModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter3 = new FirebaseRecyclerAdapter<QnAModel, QnAViewHolder>(options) {
            @Override
            public QnAViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_qna_custom_layout, parent, false);

                return new QnAViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(final QnAViewHolder holder, final int position, final QnAModel model) {

                holder.setQuestion(model.getQuestion());
                holder.setAnswer(model.getAnswer());
                holder.setButton(model.getQuestion_id(),model.getAnswer());
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (getItemCount()!=0){
                    noQnaTxt.setVisibility(View.GONE);
                    qnaList.setVisibility(View.VISIBLE);
                }else {
                    noQnaTxt.setVisibility(View.VISIBLE);
                    qnaList.setVisibility(View.GONE);
                }
                qnaProgress.setVisibility(View.GONE);
            }
        };
        adapter3.startListening();
        qnaList.setAdapter(adapter3);
    }

    private void loadReviews(){
        FirebaseRecyclerOptions<ReviewModel> options =
                new FirebaseRecyclerOptions.Builder<ReviewModel>()
                        .setQuery(reviewRef, ReviewModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter2 = new FirebaseRecyclerAdapter<ReviewModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_review_list_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(final ItemViewHolder holder, final int position, final ReviewModel model) {
                holder.setDate(model.getDate());
                holder.setName(model.getName());
                holder.setReview(model.getReview());
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (getItemCount()!=0){
                    noRwvTxt.setVisibility(View.GONE);
                    reviewList.setVisibility(View.VISIBLE);
                }else {
                    noRwvTxt.setVisibility(View.VISIBLE);
                    reviewList.setVisibility(View.GONE);
                }
                revProgress.setVisibility(View.GONE);
            }
        };
        adapter2.startListening();
        reviewList.setAdapter(adapter2);
    }

    public int setPersentageText(float numerator,float denominator) {
        int percentage = (int)(numerator * 100.0 / denominator + 0.5);
        return 100 - percentage;

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    private void getSimilarData(){
        categoryList.clear();
        final HomeHorizontalRecyclerAdapter bookDetailGridAdapter2 = new HomeHorizontalRecyclerAdapter(BookDetailActivity.this, categoryList,BookDetailActivity.this);
        Call<List<BookDataModel>> call2 = apiService.getBookByCategory(category,bookKey);
        call2.enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                for (BookDataModel result : response.body()) {
                    categoryList.add(result);
                }
                bookDetailGridAdapter2.notifyDataSetChanged();
                similarCategoryList.hideShimmerAdapter();
                similarCategoryList.setLayoutManager(new LinearLayoutManager(BookDetailActivity.this, LinearLayoutManager.HORIZONTAL, false));
                similarCategoryList.setAdapter(bookDetailGridAdapter2);
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

            }
        });
        getAuthorSimilarData();
    }

    private void getAuthorSimilarData(){
        authorList.clear();
        final HomeHorizontalRecyclerAdapter bookDetailGridAdapter1 = new HomeHorizontalRecyclerAdapter(BookDetailActivity.this, authorList,BookDetailActivity.this);
        Call<List<BookDataModel>> call = apiService.getBookByAuthor(author,bookKey);
        call.enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                for (BookDataModel result : response.body()) {
                    authorList.add(result);
                }
                bookDetailGridAdapter1.notifyDataSetChanged();
                similarAuthorList.hideShimmerAdapter();
                similarAuthorList.setLayoutManager(new LinearLayoutManager(BookDetailActivity.this, LinearLayoutManager.HORIZONTAL, false));
                similarAuthorList.setAdapter(bookDetailGridAdapter1);
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

            }
        });
    }


    void countCartItem(){
        Constant.cartRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.CartItemCount = String.valueOf(dataSnapshot.getChildrenCount());
                badge.setCount(HomeActivity.getCartItemCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent intent = new Intent(BookDetailActivity.this, BookDetailActivity.class);
        intent.putExtra("key",key);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        BookDetailActivity.this.startActivity(intent);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        View mView;


        public ItemViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }

        public void setName(String item) {
            TextView name = mView.findViewById(R.id.name);
            name.setText(Constant.upperCaseWords(item));

        }

        public void setReview(String item) {
            TextView book_name = mView.findViewById(R.id.review);
            book_name.setText(item);
        }

        public void setDate(String item) {
            TextView book_name = mView.findViewById(R.id.date);
            book_name.setText(item);
        }
    }

    public class QnAViewHolder extends RecyclerView.ViewHolder {
        View mView;


        public QnAViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }

        public void setQuestion(String item) {
            TextView name = mView.findViewById(R.id.question);
            name.setText(item);

        }

        public void setAnswer(String item) {
            TextView book_name = mView.findViewById(R.id.answer);
            if (!item.equals("null")){
                book_name.setVisibility(View.VISIBLE);
                book_name.setText(item);
            }else {
                book_name.setVisibility(View.GONE);
            }

        }

        public void setButton(final String item, String ans) {
            TextView book_name = mView.findViewById(R.id.write_ans_btn);
            if (ans.equals("null")){
                book_name.setVisibility(View.VISIBLE);
            }else {
                book_name.setVisibility(View.GONE);
            }
            book_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                    builder.setTitle("Post You Answer");
                    builder.setMessage("write your answer here.");
                    LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                    View v=li.inflate(R.layout.ask_question_dialog_layout,null,false);
                    builder.setView(v);
                    final EditText feedback = v.findViewById(R.id.question_edit_text);
                    feedback.setHint("write your answer here");
                    builder.setPositiveButton("Post Here", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (!TextUtils.isEmpty(feedback.getText().toString().trim())){
                                HashMap hm = new HashMap();
                                hm.put("answer",feedback.getText().toString());
                                qnaRef.child(item).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                                    @Override
                                    public void onComplete(@NonNull Task task) {
                                        if (task.isSuccessful()){
                                            Toast.makeText(BookDetailActivity.this, "Posted...", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }else {
                                Toast.makeText(BookDetailActivity.this, "Write somthing...", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog ad=builder.create();
                    ad.show();
                }
            });

        }
    }
}
