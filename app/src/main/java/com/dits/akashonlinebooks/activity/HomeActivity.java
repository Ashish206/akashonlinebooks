package com.dits.akashonlinebooks.activity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.extra.ApiClient;
import com.dits.akashonlinebooks.extra.ApiInterface;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.extra.CountDrawable;
import com.dits.akashonlinebooks.fragment.BookListFragment;
import com.dits.akashonlinebooks.fragment.FilterFragment;
import com.dits.akashonlinebooks.fragment.HomeDynamicFragment;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.model.SearchResultModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.messaging.FirebaseMessaging;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , InternetConnectivityListener{
    FirebaseAuth mAUth;
    public static RelativeLayout splash_screen;
    public static SearchView searchView;
    public static FragmentManager fragmentManager;
    Fragment homeDynamicFragment;
    Fragment gridFragment;
    int x=0;
    FrameLayout frameLayout;
    View gridShowView;
    ImageView cartBtn,filterBtn;
    static CountDrawable badge;
    SharedPreferences sp;
    TextView userNameTv,userPhoneTv;
    EditText feedback;
    private SimpleCursorAdapter mAdapter;
    private ApiInterface movieService;
    private ArrayList<SearchResultModel> SUGGESTIONS;
    ClearFilterListenerHome clearFilterListenerHome;
    InternetAvailabilityChecker mInternetAvailabilityChecker;
    public static Context homeContext;
    ProgressDialog progressDialog;
    ImageView notificationBtn;
    TextView refAmountTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_splash_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        homeContext = this;
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        mAUth = FirebaseAuth.getInstance();
        InternetAvailabilityChecker.init(this);

        fragmentManager = getSupportFragmentManager();

        if (mAUth.getCurrentUser() != null){

            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            Constant.useableReferralAmount = 0;
            navigationView.setNavigationItemSelectedListener(this);
            View header = navigationView.getHeaderView(0);
            userNameTv = header.findViewById(R.id.txtUserName);
            userPhoneTv = header.findViewById(R.id.txtUserNumber);
            refAmountTxt = header.findViewById(R.id.refAmountTxt);

            refAmountTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(HomeActivity.this,ReferralActivity.class));
                }
            });
            header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(HomeActivity.this,UserProfileActivity.class));
                }
            });

            frameLayout = findViewById(R.id.frame2);
            splash_screen = findViewById(R.id.splash_screen);
            splash_screen.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.GONE);
            Handler mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //start your activity here
                    HomeActivity.splash_screen.setVisibility(View.GONE);
                    mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
                    mInternetAvailabilityChecker.addInternetConnectivityListener(HomeActivity.this);
                }
            }, 3000);

            homeDynamicFragment = new HomeDynamicFragment();

            fragmentManager.beginTransaction().replace(R.id.frame,homeDynamicFragment)
                    .commit();

            Constant.cityName = sp.getString("city","");
            Constant.landMark = sp.getString("landmark","");
            Constant.stateName = sp.getString("state","");
            Constant.countryName = sp.getString("country","");
            Constant.postalCode = sp.getString("postalcode","");
            Constant.userEmail = sp.getString("email","");
            Constant.userPhone = sp.getString("phone","");
            Constant.userName = sp.getString("userName","");
            Constant.userAddress = sp.getString("address","");
            Constant.userLat = sp.getString("lat","");
            Constant.userLng = sp.getString("lng","");
            Constant.userToken = sp.getString("tokenId","");
            Constant.deliveryCharge = "0";
            Constant.BooksDetailRef = FirebaseDatabase.getInstance().getReference().child("BooksDetail");
            Constant.rootRef = FirebaseDatabase.getInstance().getReference();
            Constant.uid = FirebaseAuth.getInstance().getUid();
            Constant.userRef = FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(Constant.uid);
            Constant.cartRef = FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(Constant.uid).child("cart");
            Constant.historyRef = FirebaseDatabase.getInstance().getReference().child("UserInfo").child("Jabalpur").child(Constant.uid).child("history");
            Constant.requestBookRef = Constant.rootRef.child("Requests").child("Jabalpur");
            Constant.wishListRef = Constant.rootRef.child("UserInfo").child("Jabalpur").child(Constant.uid).child("wishlist");
            Constant.Referrals = Constant.userRef.child("Referrals");
            Constant.offerZoneRef = Constant.rootRef.child("OfferZone");

            FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
            userNameTv.setText(Constant.upperCaseWords(Constant.userName));
            userPhoneTv.setText(Constant.userPhone);

            gridShowView = findViewById(R.id.grid_show_view);

            searchView = findViewById(R.id.search_bar);
            countCartItem();
            filterBtn = findViewById(R.id.filter_btn);
            filterBtn.setVisibility(View.GONE);
            filterBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    FilterFragment filterFragment = new FilterFragment();
                    filterFragment.show(ft, "dialog");
                }
            });

            notificationBtn = findViewById(R.id.notification_btn);
            notificationBtn.setVisibility(View.VISIBLE);
            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(HomeActivity.this,NotificationActivity.class));
                }
            });

            cartBtn = findViewById(R.id.cart_btn);
            cartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(HomeActivity.this,CartActivity.class));
                }
            });

            LayerDrawable icon = (LayerDrawable) cartBtn.getDrawable();


            SUGGESTIONS = new ArrayList();
            Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
            if (reuse != null && reuse instanceof CountDrawable) {
                badge = (CountDrawable) reuse;
            } else {
                badge = new CountDrawable(this);
            }
            movieService = ApiClient.getClient().create(ApiInterface.class);

            badge.setCount(HomeActivity.getCartItemCount());
            icon.mutate();
            icon.setDrawableByLayerId(R.id.ic_group_count, badge);
            Constant.rootRef.child("DeliveryCharge").child(Constant.postalCode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild("charge")){
                        Constant.deliveryCharge = dataSnapshot.child("charge").getValue().toString();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            setReferralAmount();
            gridShowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (x==0){

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                        gridShowView.setVisibility(View.GONE);
                        searchView.requestFocus();
                        gridFragment = new BookListFragment();
                        x++;
                        fragmentManager.beginTransaction().replace(R.id.frame2,gridFragment)
                                .commit();
                        frameLayout.setVisibility(View.VISIBLE);
//                        filterBtn.setVisibility(View.VISIBLE);
                    }
                }
            });


            final String[] from = new String[] {"filter"};
            final int[] to = new int[] {android.R.id.text1};
            mAdapter = new SimpleCursorAdapter(this,
                    android.R.layout.simple_list_item_1,
                    null,
                    from,
                    to,
                    CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);


            searchView.setSuggestionsAdapter(mAdapter);

            searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                @Override
                public boolean onSuggestionSelect(int i) {

                    return false;
                }

                @Override
                public boolean onSuggestionClick(int i) {
                    Cursor cursor = (Cursor) mAdapter.getItem(i);
                    String txt = cursor.getString(cursor.getColumnIndex("filter"));
                    searchView.setQuery(txt, true);
                   // Toast.makeText(HomeActivity.this, txt, Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    getFinalSearchResult(s);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    try{
                        getSearchResultFromApi(s);

                    }catch (Exception e){

                    }

                    return false;
                }
            });

            int searchCloseButtonId = searchView.getContext().getResources()
                    .getIdentifier("android:id/search_close_btn", null, null);
            ImageView closeButton =  this.searchView.findViewById(searchCloseButtonId);

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchView.setQuery(null,false);
                    clearFilterListenerHome = BookListFragment.ctx;

                    if (clearFilterListenerHome!=null){
                        clearFilterListenerHome.ClearFilterListenerHome();
                    }
                }
            });
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(final PendingDynamicLinkData pendingDynamicLinkData) {
                            Uri deepLink = null;
                            if (pendingDynamicLinkData != null) {
                                deepLink = pendingDynamicLinkData.getLink();
                            }
                            if (deepLink != null && deepLink.getBooleanQueryParameter("bookid",false)){
                                String bookid = deepLink.getQueryParameter("bookid");
                                Intent i = new Intent(HomeActivity.this,BookDetailActivity.class);
                                i.putExtra("key",bookid);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        }
                    });

        }else {
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                            Uri deepLink = null;
                            if (pendingDynamicLinkData != null) {
                                deepLink = pendingDynamicLinkData.getLink();
                            }
                            if (deepLink != null
                                    && deepLink.getBooleanQueryParameter("invitedby", false)) {

                                String referrerUid = deepLink.getQueryParameter("invitedby");
                                Log.d("----->","deeplink");
                                Intent i = new Intent(HomeActivity.this,LoginActivity.class);
                                i.putExtra("referrerUid",referrerUid);
                                startActivity(i);
                                finish();
                            }
                            else if (deepLink == null){
                                Intent i = new Intent(HomeActivity.this,LoginActivity.class);
                                i.putExtra("referrerUid","null");
                                startActivity(i);
                                finish();
                            }else {
                                Intent i = new Intent(HomeActivity.this,LoginActivity.class);
                                i.putExtra("referrerUid","null");
                                startActivity(i);
                                finish();
                            }
                        }
                    });
        }


    }



    private void getFinalSearchResult(String s) {
        BookListFragment.adapter.clear();
        BookListFragment.isSearchResult = true;
        BookListFragment.progressBar.setVisibility(View.VISIBLE);
        getSearchDetailsResult(s).enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                if (response.body().size() !=0){
                    for(BookDataModel s : response.body()){
                        BookListFragment.adapter.add(s);
                    }
//                    showNoItemTextListener.showNoItemText(false);
                    BookListFragment.isLastPage = true;
                    BookListFragment.adapter.notifyDataSetChanged();
                    BookListFragment.adapter.checkListCount();
                    BookListFragment.progressBar.setVisibility(View.GONE);
                }else {
//                    showNoItemTextListener.showNoItemText(true);
                    BookListFragment.adapter.checkListCount();
                }
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                Log.d("----------->",t.getMessage());
               // Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSearchResultFromApi(String key){
        SUGGESTIONS.clear();
        getSearchResult(key).enqueue(new Callback<List<SearchResultModel>>() {
            @Override
            public void onResponse(Call<List<SearchResultModel>> call, Response<List<SearchResultModel>> response) {
                for(SearchResultModel s : response.body()){
                    SUGGESTIONS.add(s);
                }
                populateAdapter();
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<SearchResultModel>> call, Throwable t) {
                t.printStackTrace();
                //Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void populateAdapter() {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "filter" });
        for (int i=0; i<SUGGESTIONS.size(); i++) {
                c.addRow(new Object[] {i, SUGGESTIONS.get(i).getBookName()});
        }
        mAdapter.changeCursor(c);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (x>0){
                searchView.setQuery("",false);
                gridShowView.setVisibility(View.VISIBLE);
                searchView.clearFocus();
                fragmentManager.beginTransaction().remove(gridFragment).commit();
                frameLayout.setVisibility(View.GONE);
                filterBtn.setVisibility(View.GONE);
                x--;
            }else {
                super.onBackPressed();
            }



        }
    }


    private Call<List<SearchResultModel>> getSearchResult(String searchKey) {
        return movieService.getSearchResult(
                searchKey
        );
    }

    private Call<List<BookDataModel>> getSearchDetailsResult(String searchKey) {
        return movieService.getSearchDetailsResult(
                searchKey
        );
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_cart) {
            startActivity(new Intent(HomeActivity.this,CartActivity.class));
        } else if (id == R.id.nav_history) {
            startActivity(new Intent(HomeActivity.this,HistoryActivity.class));
        } else if (id == R.id.nav_share) {
            shareApp();
        } else if (id == R.id.nav_contact_us) {
            contactUs();
        }else if (id == R.id.nav_rate) {
            rateThisApp();
        }else if (id == R.id.nav_feedback) {
            FeedBack();
        }else if (id == R.id.nav_signout) {
            signoutBox();
        }else if (id == R.id.nav_tac) {
            tandc();
        }else if (id == R.id.nav_return) {
            returnPolicy();
        }
//        else if (id == R.id.nav_offer_zone) {
//            startActivity(new Intent(HomeActivity.this,OfferActivity.class));
//        }
        else if (id == R.id.nav_wishlist) {
            startActivity(new Intent(HomeActivity.this,WistListActivity.class));
        }
        else if (id == R.id.nav_request) {
            startActivity(new Intent(HomeActivity.this,RequestBookActivity.class));
        }
        else if (id == R.id.nav_rewards) {
            startActivity(new Intent(HomeActivity.this,ReferralActivity.class));
        }
        else {
            Intent i = new Intent(this, MoreBtnActivity.class);
            i.putExtra("category",item.getTitle());
            i.putExtra("heading",item.getTitle());
            startActivity(i);
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void countCartItem() {
        Constant.cartRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.CartItemCount = String.valueOf(dataSnapshot.getChildrenCount());
                badge.setCount(HomeActivity.getCartItemCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static String getCartItemCount() {
        return Constant.CartItemCount;
    }

    private void signoutBox() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("Do you want to sign out?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        signOut();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void returnPolicy() {
        LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View v=li.inflate(R.layout.return_policy_dialog,null,false);

        WebView webview = v.findViewById(R.id.webview);
        webview.loadUrl("https://akashpustaksadan.com/privacypolicy.html");
        AlertDialog.Builder abd=new AlertDialog.Builder(this);
        abd.setView(v);
        abd.setTitle("Return Policy");

        abd.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog ad=abd.create();
        ad.show();
    }

    private void tandc() {
        LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View v = li.inflate(R.layout.terms_and_conditions, null, false);
        WebView webview = v.findViewById(R.id.webview);
        webview.loadUrl("https://akashpustaksadan.com/termsandcondition.html");
        AlertDialog.Builder abd = new AlertDialog.Builder(this);
        abd.setView(v);
        abd.setTitle("Terms and Conditions");

        abd.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog ad = abd.create();
        ad.show();
    }

    private void contactUs() {
        LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View v=li.inflate(R.layout.contact_us_dialog,null,false);
        LinearLayout mailUs = v.findViewById(R.id.mailUsLayout);
        LinearLayout callUs = v.findViewById(R.id.callUsLayout);

        mailUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Intent.ACTION_SEND);
                in.setType("message/rfc822");
                in.putExtra(Intent.EXTRA_EMAIL  , new String[]{"akashonlinebooks@gmail.com"});
                in.putExtra(Intent.EXTRA_SUBJECT, "");
                in.putExtra(Intent.EXTRA_TEXT   , "");
                in.setPackage("com.google.android.gm");

                try {
                    startActivity(Intent.createChooser(in, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(HomeActivity.this, "There are no email clients installed.",Toast.LENGTH_SHORT).show();
                }
            }
        });


        callUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "+917440905555"));
                startActivity(intent);
            }
        });

        AlertDialog.Builder abd=new AlertDialog.Builder(this);
        abd.setView(v);
        AlertDialog ad=abd.create();
        ad.show();
    }

    private void signOut() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(HomeActivity.this,LoginActivity.class);
        intent.putExtra("referrerUid","null");
        startActivity(intent);
        finish();
    }

    public void FeedBack() {
        LayoutInflater li= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View v=li.inflate(R.layout.feedback_layout,null,false);
        AlertDialog.Builder abd=new AlertDialog.Builder(this);
        abd.setView(v);
        abd.setTitle("Feedback");
        abd.setCancelable(false);
        abd.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                feedback = v.findViewById(R.id.feedtext);
                String feedbackstr = feedback.getText().toString().trim();
                if (feedbackstr.isEmpty()) {
                    Toast.makeText(HomeActivity.this, "write something", Toast.LENGTH_SHORT).show();
                } else {
                    DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
                    final String date = dateFormat.format(Calendar.getInstance().getTime());
                    HashMap hm = new HashMap();
                    hm.put("name",Constant.userName);
                    hm.put("uid",Constant.uid);
                    hm.put("date",date);
                    hm.put("feedback",feedbackstr);

                   String key  = Constant.rootRef.child("FeedBack").push().getKey();
                    Constant.rootRef.child("FeedBack").child(key).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                Toast.makeText(HomeActivity.this, "Thanks for giving feedback.", Toast.LENGTH_SHORT).show();
                            }   else {
                                Toast.makeText(HomeActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
            }
        });

        abd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        final AlertDialog ad=abd.create();
        ad.show();

    }

    private void shareApp() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        String subject = "Download this app";
        share.putExtra(Intent.EXTRA_SUBJECT, subject);
        share.putExtra(Intent.EXTRA_TEXT, "Do you want to buy books? Just download this amazing app and get your item at your door step. " +"https://play.google.com/store/apps/details?id=" + getPackageName());
        startActivity(Intent.createChooser(share, "Share link!"));
    }

    public void rateThisApp() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setReferralAmount(){
        Constant.userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Constant.referralAmount =Integer.valueOf(dataSnapshot.child("referralAmount").getValue().toString());
                refAmountTxt.setText("Wallet Amount : ₹"+Constant.referralAmount);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Constant.isNetworkConnected = isConnected;
        if (!Constant.isNetworkConnected){
           startActivity(new Intent(HomeActivity.this,NoInternetActivity.class));
        }
    }

    private void openWhatsApp(String number) {
        try {
            number = number.replace(" ", "").replace("+", "");

            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number)+"@s.whatsapp.net");
            // getApplication().startActivity(sendIntent);

            startActivity(Intent.createChooser(sendIntent, "Compartir en")
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

        } catch(Exception e) {
            Log.e("WS", "ERROR_OPEN_MESSANGER"+e.toString());
        }
    }

    public void opneFb(View view) {
        String facebookId = "fb://page/177947582276238";
        String urlPage = "https://www.facebook.com/akashpustaksadan.jabalpur";

        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookId)));
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlPage)));
        }
    }

    public void openWhatsapp(View view) {
        openWhatsApp("+917440905555");
    }



    public interface ClearFilterListenerHome {
        void ClearFilterListenerHome();
    }
}

