package com.dits.akashonlinebooks.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.adapter.CartAdapter;
import com.dits.akashonlinebooks.extra.ApiClient;
import com.dits.akashonlinebooks.extra.ApiInterface;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.extra.GooglePay;
import com.dits.akashonlinebooks.extra.Notification;
import com.dits.akashonlinebooks.extra.jsonparse;
import com.dits.akashonlinebooks.fragment.PaymentFragment;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.razorpay.Checkout;
import com.razorpay.Order;
import com.razorpay.PaymentResultListener;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;

public class CartActivity extends AppCompatActivity implements CartAdapter.ItemListener,CartAdapter.RemoveCartItem,CartAdapter.EditCartItem, PaytmPaymentTransactionCallback, PaymentResultListener {

    String GOOGLE_PAY_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";
    int GOOGLE_PAY_REQUEST_CODE = 123;
    RecyclerView cartList;
    int i = 0;
    long cartItemCount = 0;
    ArrayList<BookDataModel> cartListArray;
    CartAdapter cartAdapter;
    LinearLayout noItemText;
    public static TextView TotalAmount;
    CardView card;
    public static float totalAmount;
    public static float totalActualAmount;
    Button shopNowBtn;
    public static Button proceedBtn;
    int screen = 0;
    PaymentFragment paymentFragment;
    FragmentManager fragmentManager;
    ProgressDialog progressDialog;
    long childrenCount = 0;
    long loopCount = 0;
    String paymentMethod = "";
    int orderStage = 0;
    ValueEventListener listener,listener2;
    DatabaseReference ref2;
    String keyDb;
    String orderID;
    private PaymentsClient mPaymentsClient;
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 42;
    private String deliveryType = "local";
    private String[] pinCode = {"482001","482002","482003","4820023","482004"};
    List<String> pinCodeList;
    ApiInterface apiService;
    RazorpayClient razorpayClient;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Constant.useableReferralAmount = 0;
        cartList = findViewById(R.id.cartList);
        cartList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        noItemText = findViewById(R.id.noItemTxt);
        TotalAmount = findViewById(R.id.totalText);
        shopNowBtn = findViewById(R.id.shop_now_btn);
        shopNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        card= findViewById(R.id.card);

        cartListArray = new ArrayList();

        ref2 = FirebaseDatabase.getInstance().getReference();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        mPaymentsClient =
                Wallet.getPaymentsClient(
                        this,
                        new Wallet.WalletOptions.Builder()
                                .setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
                                .build());
        Checkout.preload(getApplicationContext());


        pinCodeList = Arrays.asList(pinCode);
        proceedBtn = findViewById(R.id.proceedBtn);
        proceedBtn.setEnabled(false);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Constant.userAddress.equals("")){
                    if (proceedBtn.getText().toString().toLowerCase().equals("proceed next")){
                        paymentFragment = new PaymentFragment();
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().add(R.id.userDetailFragment,paymentFragment).commit();
                        screen++;
                        proceedBtn.setText("Place Order");
                    }else {
                        if (Constant.paymentMethod!= -1){
                            if (Constant.paymentMethod == 0){
                                paymentMethod = "Google Pay";
                                if(pinCodeList.contains(Constant.postalCode)){
                                    deliveryType = "local";
                                }else {
                                    deliveryType = "courier";
                                }

                                try{

                                    googlePay((int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount));
                                }catch (Exception e){
                                    Toast.makeText(CartActivity.this, "Something went wrong while opening GooglePay.Please try another payment option", Toast.LENGTH_SHORT).show();
                                }
                                
                            }else if (Constant.paymentMethod == 1){
                                paymentMethod = "Paytm";
                                if(pinCodeList.contains(Constant.postalCode)){
                                    deliveryType = "local";
                                }else {
                                    deliveryType = "courier";
                                }
                                try{
                                    sendUserDetailTOServerdd dl = new sendUserDetailTOServerdd();
                                    dl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                }catch (Exception e){
                                    Toast.makeText(CartActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                }

                            }else if (Constant.paymentMethod == 2){
                                paymentMethod = "RazorPay";
                                if(pinCodeList.contains(Constant.postalCode)){
                                    deliveryType = "local";
                                }else {
                                    deliveryType = "courier";
                                }
                                try{
                                    startRazorPayPayment();
                                }catch (Exception e){
                                    Toast.makeText(CartActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                }

                            }else if (Constant.paymentMethod == 3){
                                paymentMethod = "Paytm";
                                if(pinCodeList.contains(Constant.postalCode)){
                                    deliveryType = "local";
                                }else {
                                    deliveryType = "courier";
                                }
                                try{
                                    sendUserDetailTOServerdd dl = new sendUserDetailTOServerdd();
                                    dl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                }catch (Exception e){
                                    Toast.makeText(CartActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                }

                            }else {
                                paymentMethod = "Cash On Delivery";
                                if(pinCodeList.contains(Constant.postalCode)){
                                    deliveryType = "local";
                                }else {
                                    deliveryType = "courier";
                                }
                                orderPlaced();
                            }
                        }else {
                            Toast.makeText(CartActivity.this, "Select Payment Method", Toast.LENGTH_SHORT).show();
                        }
                    }

                }else {
                    startActivity(new Intent(CartActivity.this,ChangeAddressActivity.class));
                }

            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        loadCartList();
    }

    void loadCartList(){
        if (!progressDialog.isShowing()){
            progressDialog.show();
        }
        Constant.cartRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                cartListArray.clear();
                cartItemCount = dataSnapshot.getChildrenCount();
                if (cartItemCount!=0) {
                    totalAmount = 0;
                    totalActualAmount = 0;
                    card.setVisibility(View.VISIBLE);
                    noItemText.setVisibility(View.GONE);
                    cartList.setVisibility(View.VISIBLE);
                    for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                        final ArrayList<BookDataModel> bookDetailList = new ArrayList<>();


                        Call<List<BookDataModel>> call = apiService.getSingleBookDetails(ds.child("itemUid").getValue().toString());
                        call.enqueue(new Callback<List<BookDataModel>>() {
                            @Override
                            public void onResponse(Call<List<BookDataModel>> call, retrofit2.Response<List<BookDataModel>> response) {

                                for (BookDataModel result : response.body()) {
                                    bookDetailList.add(result);
                                }
                                if (bookDetailList.get(0).getQuantity() >= Integer.valueOf(ds.child("quantity").getValue().toString())){

                                    BookDataModel bookDataModel = new BookDataModel();
                                    bookDataModel.setBookName(bookDetailList.get(0).getBookName());
                                    bookDataModel.setImage(bookDetailList.get(0).getImage());
                                    bookDataModel.setAuthor(bookDetailList.get(0).getAuthor());
                                    bookDataModel.setPublisher(bookDetailList.get(0).getPublisher());
                                    bookDataModel.setMRP(bookDetailList.get(0).getMRP());
                                    bookDataModel.setActualCost(bookDetailList.get(0).getActualCost());
                                    bookDataModel.setBookKey(bookDetailList.get(0).getBookKey());
                                    bookDataModel.setBookQuantity(bookDetailList.get(0).getQuantity());
                                    bookDataModel.setQuantity(Integer.valueOf(ds.child("quantity").getValue().toString()));
                                    totalAmount = totalAmount + (Float.parseFloat(bookDetailList.get(0).getMRP()) * Float.parseFloat(ds.child("quantity").getValue().toString()));
                                    totalActualAmount = totalActualAmount + (Float.parseFloat(bookDetailList.get(0).getActualCost()) * Float.parseFloat(ds.child("quantity").getValue().toString()));
                                    cartListArray.add(bookDataModel);

                                }else {
                                    removeBooksFromCart(ds.child("itemUid").getValue().toString());
                                }
                                i++;
                                if (cartItemCount == i) {
                                    setData();
                                    i = 0;
                                }


                            }

                            @Override
                            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

                            }
                        });
                    }
                }else {
                    card.setVisibility(View.GONE);
                    cartList.removeAllViews();
                    cartList.setVisibility(View.GONE);
                    noItemText.setVisibility(View.VISIBLE);
                    if (progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Constant.isAddressChanged){
            Constant.isAddressChanged = false;
            startActivity(new Intent(CartActivity.this,CartActivity.class));
            finish();
        }
    }

    private void setData() {

        cartList.removeAllViews();
        cartAdapter = new CartAdapter(getApplicationContext(),cartListArray,CartActivity.this);
        cartAdapter.setData(cartListArray);
        cartList.setAdapter(cartAdapter);
        cartAdapter.notifyDataSetChanged();
//        if (totalAmount!=0 && totalAmount>=250 && Constant.referralAmount>=10){
//            if (((Constant.referralAmount*20)/100)>=200){
//                Constant.useableReferralAmount = 200;
//            }else {
//                Constant.useableReferralAmount = ((Constant.referralAmount*20)/100);
//            }
//
//        }else {
//            Constant.useableReferralAmount=0;
//        }

        if (totalAmount!=0 && totalAmount>=100 && Constant.referralAmount>=10){
            if (Constant.referralAmount>=50){
                Constant.useableReferralAmount = 50;
            }else {
                Constant.useableReferralAmount = Constant.referralAmount;
            }
        }else {
            Constant.useableReferralAmount=0;
        }
        int x =  (int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount);
        TotalAmount.setText("₹"+x);
        if (progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        if (screen == 0){
            super.onBackPressed();
        }
        else {
            screen--;
            Constant.paymentMethod = -1;
            fragmentManager.beginTransaction().remove(paymentFragment).commit();
            proceedBtn.setText("Proceed Next");
        }

    }

    @Override
    public void onItemClick(String key, ImageView imageView) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        Constant.breaker = 1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.breaker = 1;
//        setData();
        orderID = Constant.historyRef.push().getKey();
    }

    @Override
    public void onRemoveItemClick(String key) {
       removeBooksFromCart(key);
    }

    private void removeBooksFromCart(String key){
        Constant.cartRef.child(key).setValue(null);
        cartList.removeAllViews();
        loadCartList();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onEditCartItemListener(final String key,Integer quantity) {

        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        d.setTitle("Edit Order");
        d.setMessage("Change the quantity of your product");
        d.setView(dialogView);
        final NumberPicker numberPicker = dialogView.findViewById(R.id.dialog_number_picker);
        numberPicker.setMaxValue(quantity);
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {

            }
        });
        d.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Constant.cartRef.child(key).child("quantity").setValue(String.valueOf(numberPicker.getValue())).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressDialog.show();
                        cartList.removeAllViews();
                        loadCartList();
                    }
                });
            }
        });
        d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alertDialog = d.create();
        alertDialog.show();

    }

    private void googlePay(int total) {

//        GooglePay.totalGooglePayAmount = String.valueOf(total);
//        final Optional<JSONObject> isReadyToPayJson = GooglePay.getIsReadyToPayRequest();
//        if (!isReadyToPayJson.isPresent()) {
//            return;
//        }
//        IsReadyToPayRequest request = IsReadyToPayRequest.fromJson(isReadyToPayJson.get().toString());
//        if (request == null) {
//            return;
//        }
//        Task<Boolean> task = mPaymentsClient.isReadyToPay(request);
//        task.addOnCompleteListener(
//                new OnCompleteListener<Boolean>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Boolean> task) {
//                        try {
//                            boolean result = task.getResult(ApiException.class);
//                            if (result) {
//                                requestPayment();
//                            }
//                        } catch (ApiException exception) {
//                            // handle developer errors
//                            Log.d("-------->",exception.getMessage());
//                        }
//                    }
//                });

  //paytmqr281005050101809ywssbyv31@paytm

        //paytmqr281005050101i2ar71p8vfsc@paytm
        Uri uri =
                new Uri.Builder()
                        .scheme("upi")
                        .authority("pay")
                        .appendQueryParameter("pa", "7440805555@okbizaxis")
                        .appendQueryParameter("pn", "Akash Online Books")
                        .appendQueryParameter("tr", String.valueOf(randInt(10,10)))
                        .appendQueryParameter("tn", "Akash Online Books")
                        .appendQueryParameter("am", String.valueOf(total))
                        .appendQueryParameter("cu", "INR")
                        .build();


        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);
        intent.setPackage(GOOGLE_PAY_PACKAGE_NAME);
        startActivityForResult(intent, GOOGLE_PAY_REQUEST_CODE);
    }

    public void requestPayment() {
        Optional<JSONObject> paymentDataRequestJson = GooglePay.getPaymentDataRequest();
        if (!paymentDataRequestJson.isPresent()) {
            return;
        }
        PaymentDataRequest request =
                PaymentDataRequest.fromJson(paymentDataRequestJson.get().toString());
        if (request != null) {
            AutoResolveHelper.resolveTask(
                    mPaymentsClient.loadPaymentData(request), this, LOAD_PAYMENT_DATA_REQUEST_CODE);
        }
    }


    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // value passed in AutoResolveHelper
            case LOAD_PAYMENT_DATA_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        PaymentData paymentData = PaymentData.getFromIntent(data);
                        String json = paymentData.toJson();
                        // if using gateway tokenization, pass this token without modification
                        JSONObject paymentMethodData = null;
                        String paymentToken = null;
                        try {
                            paymentMethodData = new JSONObject(json)
                                    .getJSONObject("paymentMethodData");
                            paymentToken = paymentMethodData
                                    .getJSONObject("tokenizationData")
                                    .getString("token");
                            orderPlaced();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                    case Activity.RESULT_CANCELED:
                        showOrderUnSuccessfulDialog();
                        break;
                    case AutoResolveHelper.RESULT_ERROR:
                        Status status = AutoResolveHelper.getStatusFromIntent(data);
                        showOrderUnSuccessfulDialog();
                        // Log the status for debugging.
                        // Generally, there is no need to show an error to the user.
                        // The Google Pay payment sheet will present any account errors.
                        break;
                    default:
                        // Do nothing.
                }
                break;
            default:
                // Do nothing.
        }

        if (requestCode == GOOGLE_PAY_REQUEST_CODE) {
            try{
                if (data.getStringExtra("Status").toLowerCase().equals("failure")){
                    showOrderUnSuccessfulDialog();
                }
                else {
                    orderPlaced();
                }
            }catch (Exception e){
                showOrderUnSuccessfulDialog();
                e.printStackTrace();
            }

        }
    }


    void orderPlaced(){
        progressDialog.show();

        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        final String today = formatter.format(Calendar.getInstance().getTime());



        DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
        final String date = dateFormat.format(Calendar.getInstance().getTime());

        DateFormat timeForamt = new SimpleDateFormat("h:mm a");
        final String time = timeForamt.format(Calendar.getInstance().getTime());

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Orders").child("Jabalpur").child("Orders");
        final String str = ref.push().getKey();
        final String key1 =  Constant.historyRef.push().getKey();
        final HashMap hm = new HashMap();
        hm.put("order_id",str);
        hm.put("date",date.replace("'",""));
        hm.put("time",time);
        hm.put("deliveryBoyName","Not Selected");
        hm.put("deliveryBoyNumber","number");
        hm.put("delivered","Order Placed");
        hm.put("userId",Constant.uid);
        hm.put("userName",Constant.userName);
        hm.put("userPhone",Constant.userPhone);
        hm.put("userEmail",Constant.userEmail);
        hm.put("userState",Constant.stateName);
        hm.put("userCity",Constant.cityName);
        hm.put("userAddress",Constant.userAddress.replace("'",""));
        hm.put("userLandmark",Constant.landMark.replace("'",""));
        hm.put("userPostalCode",Constant.postalCode);
        hm.put("userLat",Constant.userLat);
        hm.put("userTokenId",Constant.userToken);
        hm.put("orderIdUser",key1);
        hm.put("filterDate",today);
        hm.put("deliveryType",deliveryType);
        hm.put("pickedBy","-");
        hm.put("packedBy","-");
        hm.put("pickedDateTime","-");
        hm.put("packedDateTime","-");
        hm.put("deliveredDateTime","-");
        hm.put("userLng",Constant.userLng);
        hm.put("paymentMethod",paymentMethod);
        hm.put("referralAmount",String.valueOf(Constant.useableReferralAmount));
        hm.put("deliveryCharge",String.valueOf(Integer.valueOf(Constant.deliveryCharge)));
        hm.put("TotalAmount",String.valueOf((int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount)));
        hm.put("ActualTotalAmount",String.valueOf((int)totalActualAmount));
        ref.child(str).updateChildren(hm).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull final Task<Void> task) {
                if (task.isSuccessful()){
                    orderStage=1;

                    listener2 = new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (final DataSnapshot ds : dataSnapshot.getChildren()) {
                                keyDb = ds.child("itemUid").getValue().toString();
                                final String key = ref.child(str).push().getKey();
                                childrenCount = dataSnapshot.getChildrenCount();

                                ApiInterface apiService =
                                        ApiClient.getClient().create(ApiInterface.class);

                                Call<List<BookDataModel>> call = apiService.getSingleBookDetails(ds.child("itemUid").getValue().toString());
                                call.enqueue(new Callback<List<BookDataModel>>() {
                                    @Override
                                    public void onResponse(Call<List<BookDataModel>> call, retrofit2.Response<List<BookDataModel>> response) {

                                        for (final BookDataModel result : response.body()) {
                                            if (Constant.breaker == 1){
                                                final HashMap hm2 = new HashMap();
                                                hm2.put("order_id",str);
                                                hm2.put("BookName",result.getBookName().replace("'",""));
                                                hm2.put("Image",result.getImage());
                                                hm2.put("Author",result.getAuthor().replace("'",""));
                                                hm2.put("Publisher",result.getPublisher().replace("'",""));
                                                hm2.put("MRP",result.getMRP());
                                                hm2.put("isbn",result.getIsbn());
                                                hm2.put("actualCost",result.getActualCost());
                                                hm2.put("key",result.getBookKey());
                                                hm2.put("Rated","false");
                                                hm2.put("Category",result.getCategory().replace("'",""));
                                                hm2.put("reviewed","false");
                                                hm2.put("historyref","false");
                                                hm2.put("quantity",ds.child("quantity").getValue().toString());
                                                hm2.put("Amount",(Float.parseFloat(result.getMRP()) * Float.parseFloat(ds.child("quantity").getValue().toString())));
                                                hm2.put("ActualAmount",(Float.parseFloat(result.getActualCost()) * Float.parseFloat(ds.child("quantity").getValue().toString())));
                                                hm2.put("product_id",key);
                                                ref.child(str).child(key).updateChildren(hm2).addOnCompleteListener(new OnCompleteListener() {
                                                    @Override
                                                    public void onComplete(@NonNull Task task) {
                                                        if (task.isSuccessful()){
                                                            deductQuantity(result.getBookKey());
                                                            orderStage=2;

                                                            Constant.historyRef.child(key1).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                                                                @Override
                                                                public void onComplete(@NonNull Task task) {
                                                                    if (task.isSuccessful()){
                                                                        orderStage = 3;
                                                                        String key2 =  Constant.historyRef.child(key1).push().getKey();
                                                                        Constant.historyRef.child(key1).child(key2).updateChildren(hm2).addOnCompleteListener(new OnCompleteListener() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task task) {
                                                                                if (task.isSuccessful()){
                                                                                    orderStage = 4;
                                                                                    hm.clear();
                                                                                    hm2.clear();
                                                                                    loopCount++;

                                                                                    if (childrenCount == loopCount){
                                                                                        progressDialog.dismiss();
                                                                                        orderPlacedSuccessfully();
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    progressDialog.dismiss();
                                                                                    Toast.makeText(CartActivity.this, "Opps!! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                    else {
                                                                        progressDialog.dismiss();
                                                                        Toast.makeText(CartActivity.this, "Opps!! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            });


                                                        }else {
                                                            progressDialog.dismiss();
                                                            Toast.makeText(CartActivity.this, "Opps!! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

                                    }
                                });


                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    };

                    Constant.cartRef.addListenerForSingleValueEvent(listener2);


                }else {
                    progressDialog.dismiss();
                    Toast.makeText(CartActivity.this, "Opps!! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void orderPlacedSuccessfully() {
        if (orderStage ==4){
            new Notification(this).sendNotificationToSubs("New Order Alert","You have received a new order. Please accept it.","Picking");
            sendSms("Your Order Of Rs+"+String.valueOf((int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount))+" has placed. Thanks for shopping with us.");
            Constant.cartRef.setValue(null);
            Constant.userRef.child("referralAmount").setValue(String.valueOf(Constant.referralAmount-Constant.useableReferralAmount));
            Constant.useableReferralAmount = 0;

            showOrderSuccessfulDialog();
        }else {
            showOrderUnSuccessfulDialog();
        }

    }

    private void showOrderSuccessfulDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.order_successful,null,false);
        builder.setView(v);
        builder.setCancelable(false);
        builder.setPositiveButton("Continue Shopping", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();

    }

    private void deductQuantity(String bookKey){
        Constant c = new Constant();
        c.getDataFromServer(this, Request.Method.GET, "deductQuantity/?bookKey='"+bookKey+"'", new Constant.OnResponseFromServer() {
            @Override
            public void onEvent(String response) {
                Log.d("------>",response);
            }
        },null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            Constant.paymentMethod =-1;
            ref2.child("BooksDetail").child(keyDb).removeEventListener(listener);
            Constant.cartRef.removeEventListener(listener2);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showOrderUnSuccessfulDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = li.inflate(R.layout.order_unsuccessful_dialog,null,false);
        builder.setView(v);
        builder.setCancelable(false);
        builder.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private void sendSms(String smstext) {
        String url = "https://api.msg91.com/api/sendhttp.php?country=91&sender=AOBJBP&route=4&mobiles="+Constant.userPhone+"&authkey=106940A18cEbcPcZ75c503f65&message="+smstext+
                " will reach you shortly %0a%0aRegards,%0aAkash Online Books";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(request);

    }


    public void startRazorPayPayment() {
        try {
            razorpayClient = new RazorpayClient("rzp_live_BT8c8h3RHKbvDd", "ABZN5U5AzxLP5hRqPMgmdCTD");
            JSONObject orderRequest = new JSONObject();
            try {
                orderRequest.put("amount", (int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount)*100); // amount in the smallest currency unit
                orderRequest.put("currency", "INR");
                orderRequest.put("receipt", orderID);
                orderRequest.put("payment_capture", true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Order order = razorpayClient.Orders.create(orderRequest);
            createRazorPayPament(order);
            Log.d("----------->",order.toString());
        } catch (RazorpayException e) {
            Log.d("----------->",e.getMessage());
        }
    }

    private void createRazorPayPament(Order order) {
        Checkout checkout = new Checkout();
        checkout.setKeyID("rzp_live_BT8c8h3RHKbvDd");
        checkout.setImage(R.mipmap.ic_launcher);

        final Activity activity = this;
        try {
            JSONObject options = new JSONObject();
            options.put("name", "Akash Online Books");
            options.put("description", orderID);
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("order_id", order.toJson().optString("id"));
            options.put("prefill.email",Constant.userEmail);
            options.put("prefill.contact",Constant.userPhone);
            options.put("currency", "INR");
            options.put("amount", (int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount)*100);
            checkout.open(activity, options);
        } catch(Exception e) {
            Log.e("---->", "Error in starting Razorpay Checkout", e);
        }
    }

    public class sendUserDetailTOServerdd extends AsyncTask<ArrayList<String>, Void, String> {
        private ProgressDialog dialog = new ProgressDialog(CartActivity.this);
        //private String orderId , mid, custid, amt;
        String url ="https://daviditsolution.in/paytm/generateChecksum.php";
        String varifyurl = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
       // String varifyurl = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
        // "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID"+orderId;
        String CHECKSUMHASH ="";
        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }
        protected String doInBackground(ArrayList<String>... alldata) {
            jsonparse jsonParser = new jsonparse(CartActivity.this);
            String param=
                    "MID="+Constant.mid+
                            "&ORDER_ID=" + orderID+
                            "&CUST_ID="+Constant.uid+
                            "&CHANNEL_ID=WAP&TXN_AMOUNT="+(int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount)+"&WEBSITE=DEFAULT"+
                            "&CALLBACK_URL=https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderID+"&INDUSTRY_TYPE_ID=Retail";
            JSONObject jsonObject = jsonParser.makeHttpRequest(url,"POST",param);
            // yaha per checksum ke saht order id or status receive hoga..
            Log.e("CheckSum result >>",jsonObject.toString());
            if(jsonObject != null){
                Log.e("CheckSum result >>",jsonObject.toString());
                try {
                    CHECKSUMHASH=jsonObject.has("CHECKSUMHASH")?jsonObject.getString("CHECKSUMHASH"):"";
                    Log.e("CheckSum result >>",CHECKSUMHASH);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return CHECKSUMHASH;
        }
        @Override
        protected void onPostExecute(String result) {
            Log.e(" setup acc ","  signup result  " + result);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            PaytmPGService Service = PaytmPGService.getProductionService();
            // when app is ready to publish use production service
            // PaytmPGService  Service = PaytmPGService.getProductionService();
            // now call paytm service here
            //below parameter map is required to construct PaytmOrder object, Merchant should replace below map values with his own values
            HashMap<String, String> paramMap = new HashMap<String, String>();
            //these are mandatory parameters
            paramMap.put("MID", Constant.mid); //MID provided by paytm
            paramMap.put("ORDER_ID", orderID);
            paramMap.put("CUST_ID", Constant.uid);
            paramMap.put("CHANNEL_ID", "WAP");
            paramMap.put("TXN_AMOUNT", String.valueOf((int)(totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount)));
            paramMap.put("WEBSITE", "DEFAULT");
            paramMap.put("CALLBACK_URL" ,"https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderID);
            //paramMap.put( "EMAIL" , "abc@gmail.com");   // no need
            // paramMap.put( "MOBILE_NO" , "9144040888");  // no need
            paramMap.put("CHECKSUMHASH" ,CHECKSUMHASH);
            //paramMap.put("PAYMENT_TYPE_ID" ,"CC");    // no need
            paramMap.put("INDUSTRY_TYPE_ID", "Retail");
            PaytmOrder Order = new PaytmOrder(paramMap);
            Log.e("checksum ", "param "+ paramMap.toString());
            Service.initialize(Order,null);
            // start payment service call here
            Service.startPaymentTransaction(CartActivity.this, true, true,
                    CartActivity.this  );
        }
    }

    @Override
    public void onTransactionResponse(Bundle bundle) {
        Log.e("checksum-- ", " respon true " + bundle.toString());
        if (bundle.getString("STATUS").equals("TXN_SUCCESS")){
            orderPlaced();
        }else {
            Toast.makeText(this, bundle.getString("RESPMSG"), Toast.LENGTH_SHORT).show();
            showOrderUnSuccessfulDialog();
        }

    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "No Network Available", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Log.e("checksum-- ", " ui fail respon  "+ s );
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Log.e("checksum-- ", " error loading pagerespon true "+ s + "  s1 " + s1);
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Log.e("checksum-- ", " cancel call back respon  " );
        showOrderUnSuccessfulDialog();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Log.e("checksum-- ", "  transaction cancel " );
        showOrderUnSuccessfulDialog();
    }

    @Override
    public void onPaymentSuccess(String s) {
        orderPlaced();

    }

    @Override
    public void onPaymentError(int i, String s) {
        showOrderUnSuccessfulDialog();
    }

}
