package com.dits.akashonlinebooks.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.HistoryListModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.kofigyan.stateprogressbar.StateProgressBar;

public class HistoryActivity extends AppCompatActivity {

    RecyclerView history_list;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog progressDialog;
    LinearLayout noItemLayout;
    Button shopNowBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);


        noItemLayout = findViewById(R.id.noItemLayout);
        noItemLayout.setVisibility(View.GONE);
        shopNowBtn = findViewById(R.id.shop_now_btn);
        shopNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        history_list = findViewById(R.id.history_list);
        history_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        history_list.setLayoutManager(linearLayoutManager);
        history_list.setHasFixedSize(true);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Your Orders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        loadHistory();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Constant.breaker = 0;

    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.breaker = 0;
    }

    private void loadHistory() {
        FirebaseRecyclerOptions<HistoryListModel> options = new FirebaseRecyclerOptions.Builder<HistoryListModel>()
                .setQuery(Constant.historyRef,HistoryListModel.class)
                .build();
        FirebaseRecyclerAdapter firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<HistoryListModel, HistoryViewHolder>(options) {
            @NonNull
            @Override
            public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.single_history_layout, viewGroup, false);

                return new HistoryViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final HistoryViewHolder holder, final int position, @NonNull final HistoryListModel model) {

                holder.setOrderDate(model.getDate());
                holder.setBookName(getRef(position).getKey());
                holder.setCost(model.getTotalAmount());
                holder.setActualCost(model.getActualTotalAmount());
                holder.setDiscount(Float.parseFloat(model.getTotalAmount())-Float.parseFloat(model.getDeliveryCharge()),Float.parseFloat(model.getActualTotalAmount()));
                holder.setDeliveryStatus(model.getDelivered());


                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Toast.makeText(HistoryActivity.this, model.getCost(), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(HistoryActivity.this,HistoryDetailActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        i.putExtra("Cost",model.getTotalAmount());
                        i.putExtra("ActualCost",model.getActualTotalAmount());
                        i.putExtra("date",model.getDate());
                        i.putExtra("delivered",model.getDelivered());
                        i.putExtra("orderKey",model.getOrder_id());
                        i.putExtra("orderref",getRef(position).getKey());
                        i.putExtra("paymentMethod",model.getPaymentMethod());
                        i.putExtra("userAddress",model.getUserAddress());
                        i.putExtra("userId",model.getUserId());
                        i.putExtra("userName",model.getUserName());
                        i.putExtra("userPhone",model.getUserPhone());
                        i.putExtra("deliveryCharge",model.getDeliveryCharge());
                        i.putExtra("referralAmount",model.getReferralAmount());
                        i.putExtra("userPostalCode",model.getUserPostalCode());
                        startActivity(i);
                        overridePendingTransition(0,0);

                    }
                });
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                if (getItemCount()!=0){
                    noItemLayout.setVisibility(View.GONE);
                }else {
                    noItemLayout.setVisibility(View.VISIBLE);
                }
                progressDialog.dismiss();
            }
        };
        firebaseRecyclerAdapter.startListening();
        history_list.setAdapter(firebaseRecyclerAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private class HistoryViewHolder extends RecyclerView.ViewHolder {
        String[] descriptionData = {"Placed", "Confirmed", "Packed", "On Way","Delivered"};
        View mView;
        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setBookName(final String item) {
            final String[] name ={""};
            final TextView item_name = mView.findViewById(R.id.book_name);
            item_name.setText(item);

            Constant.historyRef.child(item).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        if (ds.hasChild("BookName")){
                           name[0] += ds.child("BookName").getValue().toString()+", ";
                        }
                    }

                    item_name.setText(name[0]);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

        public void setOrderDate(String item) {
            TextView subtxt = mView.findViewById(R.id.order_date);
            subtxt.setText("Ordered On "+item);
        }

        public void setCost(String item) {
            TextView subtxt = mView.findViewById(R.id.cost_text);
            subtxt.setText("₹"+item);
        }

        public void setActualCost(String item) {
            TextView subtxt = mView.findViewById(R.id.actual_cost_text);
            subtxt.setText("₹"+item);
        }

        public void setDiscount(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }

        public void setDeliveryStatus(String totalCost) {
            TextView book_name = mView.findViewById(R.id.delivery_status);
            book_name.setText(totalCost);
            StateProgressBar stateProgressBar = mView.findViewById(R.id.your_state_progress_bar_id);
            stateProgressBar.setStateDescriptionData(descriptionData);

            if (totalCost.equals("Order Placed"))
            {
                book_name.setVisibility(View.GONE);
                stateProgressBar.setVisibility(View.VISIBLE);
                stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);

            }
            else if (totalCost.equals("Confirmed"))
            {
                book_name.setVisibility(View.GONE);
                stateProgressBar.setVisibility(View.VISIBLE);
                stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
            }
            else if (totalCost.equals("Order Packed"))
            {
                book_name.setVisibility(View.GONE);
                stateProgressBar.setVisibility(View.VISIBLE);
                stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
            }
            else if (totalCost.equals("On Way"))
            {
                book_name.setVisibility(View.GONE);
                stateProgressBar.setVisibility(View.VISIBLE);
                stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
            }
            else if (totalCost.equals("Cancelled"))
            {
                book_name.setVisibility(View.VISIBLE);
                stateProgressBar.setVisibility(View.GONE);
                book_name.setTextColor(getResources().getColor(R.color.red));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Delivered")){
                book_name.setVisibility(View.VISIBLE);
                stateProgressBar.setVisibility(View.GONE);
                book_name.setTextColor(getResources().getColor(R.color.green));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_dot, 0, 0, 0);
            }

        }

    }
}
