package com.dits.akashonlinebooks.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.adapter.BookDetailGridAdapter;
import com.dits.akashonlinebooks.adapter.BookGridSimpleAdapter;
import com.dits.akashonlinebooks.extra.ApiClient;
import com.dits.akashonlinebooks.extra.ApiInterface;
import com.dits.akashonlinebooks.extra.BookData;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.extra.CountDrawable;
import com.dits.akashonlinebooks.fragment.BookListFragment;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.utils.PaginationAdapterCallback;
import com.dits.akashonlinebooks.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MoreBtnActivity extends AppCompatActivity implements BookGridSimpleAdapter.ItemListener{
    SearchView searchView;
    TextView heading;

    String category,headingTxt;

    ImageView cartBtn;
    static CountDrawable badge;
    ImageView filterBtn;

    BookGridSimpleAdapter adapter;
    GridLayoutManager manager;
    RecyclerView rv;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    TextView txtError;
    SwipeRefreshLayout swipeRefreshLayout;
    private static final int PAGE_START = 1;

    private int currentPage = PAGE_START;

    private ApiInterface movieService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_btn);


        category = getIntent().getExtras().getString("category");
        headingTxt = getIntent().getExtras().getString("heading");


        filterBtn = findViewById(R.id.filter_btn);
        filterBtn.setVisibility(View.GONE);
        searchView = findViewById(R.id.search_bar);
        heading = findViewById(R.id.titleText);
        heading.setText(Constant.upperCaseWords(headingTxt));
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);

        rv = findViewById(R.id.bookList);
        progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);
        swipeRefreshLayout = findViewById(R.id.main_swiperefresh);

        rv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return false;
            }
        });

        adapter = new BookGridSimpleAdapter(this,MoreBtnActivity.this);

        manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        rv.setLayoutManager(manager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);


        //init service and load data
        movieService = ApiClient.getClient().create(ApiInterface.class);

        //loadFirstPage();

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!category.substring(0, 1).equals("'")){
                    loadDetails();
                }else {
                    loadOfferList();
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doRefresh();
            }
        });

        cartBtn = findViewById(R.id.cart_btn);
        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MoreBtnActivity.this,CartActivity.class));
            }
        });

        LayerDrawable icon = (LayerDrawable) cartBtn.getDrawable();

        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_group_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(this);
        }

        badge.setCount(HomeActivity.getCartItemCount());
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_group_count, badge);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return true;
            }
        });

        if (!category.substring(0, 1).equals("'")){
            loadDetails();
        }else {
            loadOfferList();
        }

    }

    private void loadOfferList(){
        hideErrorView();
        callOfferList().enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                hideErrorView();
                adapter.addAll(response.body());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    private void loadDetails() {
        hideErrorView();
        callTopRatedMoviesApi().enqueue(new Callback<List<BookDataModel>>() {
            @Override
            public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                hideErrorView();
                adapter.addAll(response.body());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<BookDataModel>> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        swipeRefreshLayout.requestFocus();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void doRefresh() {
        progressBar.setVisibility(View.VISIBLE);
        if (callOfferList().isExecuted())
            callOfferList().cancel();
        if (callTopRatedMoviesApi().isExecuted())
            callTopRatedMoviesApi().cancel();

        adapter.clear();
        adapter.notifyDataSetChanged();
        if (!category.substring(0, 1).equals("'")){
            loadDetails();
        }else {
            loadOfferList();
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    private Call<List<BookDataModel>> callTopRatedMoviesApi() {
        return movieService.getFilteredList(
                currentPage,
                "Category",
                category
        );
    }

    private Call<List<BookDataModel>> callOfferList() {
        return movieService.getAllOfferList(
                category
        );
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent intent = new Intent(this, BookDetailActivity.class);
        intent.putExtra("key",key);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, imageView, "image_transition");
        startActivity(intent);
    }
}
