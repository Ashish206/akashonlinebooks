package com.dits.akashonlinebooks.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.adapter.BookDetailGridAdapter;
import com.dits.akashonlinebooks.extra.ApiClient;
import com.dits.akashonlinebooks.extra.ApiInterface;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.model.WishListModel;
import com.dits.akashonlinebooks.utils.PaginationAdapterCallback;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class WistListActivity extends AppCompatActivity implements BookDetailGridAdapter.ItemListener, PaginationAdapterCallback,BookDetailGridAdapter.ShowNoItemText {


    RecyclerView recyclerView;
    ArrayList<BookDataModel> bookList;
    ArrayList<WishListModel> keys;
    TextView noDataTxt;

    private Parcelable state;
    GridLayoutManager manager;
    BookDetailGridAdapter bookDetailGridAdapter;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wist_list);

        bookList = new ArrayList<>();
        keys = new ArrayList<>();

        progressBar = findViewById(R.id.main_progress);
        progressBar.setVisibility(View.VISIBLE);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Wishlist");
        recyclerView = findViewById(R.id.recycler_view);
        noDataTxt = findViewById(R.id.noDataTxt);

        manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        bookDetailGridAdapter = new BookDetailGridAdapter(getApplicationContext(),WistListActivity.this,WistListActivity.this);
        recyclerView.setAdapter(bookDetailGridAdapter);

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        keys.clear();
//        bookList.clear();
//        showData();
//    }


    @Override
    protected void onStart() {
        super.onStart();
        showData();
    }

    public void onPause() {
        super.onPause();
        state = manager.onSaveInstanceState();
    }

    private void showData() {
        Constant.wishListRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount()!=0){
                    noDataTxt.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }else {
                    progressBar.setVisibility(View.GONE);
                    noDataTxt.setVisibility(View.VISIBLE);
                    recyclerView.removeAllViewsInLayout();
                }
                for (final DataSnapshot d : dataSnapshot.getChildren()){
                    WishListModel wishListModel = d.getValue(WishListModel.class);
                    keys.add(wishListModel);
                    ApiInterface apiService =
                            ApiClient.getClient().create(ApiInterface.class);

                    Call<List<BookDataModel>> call = apiService.getSingleBookDetails(d.child("key").getValue().toString());
                    call.enqueue(new Callback<List<BookDataModel>>() {
                        @Override
                        public void onResponse(Call<List<BookDataModel>> call, retrofit2.Response<List<BookDataModel>> response) {

                            for (BookDataModel result : response.body()) {
                                bookList.add(result);
                            }

                            if (dataSnapshot.getChildrenCount() == bookList.size()){
                                showList();


                            }
                        }

                        @Override
                        public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

                        }
                    });
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void showList(){

        if (keys.size() > 0){
            noDataTxt.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            bookDetailGridAdapter.addList(bookList);
            bookDetailGridAdapter.notifyDataSetChanged();
            manager.onRestoreInstanceState(state);
        }
        else {
            recyclerView.removeAllViewsInLayout();
            recyclerView.setVisibility(View.GONE);
            noDataTxt.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent intent = new Intent(this, BookDetailActivity.class);
        intent.putExtra("key",key);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                WistListActivity.this, imageView, "image_transition");
        startActivity(intent);
    }

    @Override
    public void retryPageLoad() {

    }

    @Override
    public void showNoItemText(boolean b) {

    }
}
