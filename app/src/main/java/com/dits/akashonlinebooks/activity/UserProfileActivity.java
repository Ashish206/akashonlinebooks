package com.dits.akashonlinebooks.activity;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.extra.Constant;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class UserProfileActivity extends AppCompatActivity {

    EditText nameField,emailField,phoneField;
    Spinner preference,study,college,course,year;
    DatabaseReference mainRef;
    TextView edittxt;
    String studyArray[] = {"Select","College","School","Coaching"};
    List<String> CollegeList;
    String studyName,collegeName = "",courceName="",yearName,preferenceName,collegeNameFromServer;
    List<String> SchoolList;
    List<String> CoachingList;
    List<String> CourseList;
    ProgressDialog progressDialog;
    int selectedStudyIn = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        edittxt = findViewById(R.id.edittxt);
        nameField = findViewById(R.id.nameField);
        emailField = findViewById(R.id.emailField);
        phoneField = findViewById(R.id.phoneField);

        CollegeList = new ArrayList();
        SchoolList = new ArrayList();
        CoachingList = new ArrayList();
        CourseList = new ArrayList();


        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        preference = findViewById(R.id.preference);
        college = findViewById(R.id.college);
        course = findViewById(R.id.course);
        study = findViewById(R.id.study);
        year = findViewById(R.id.year);

        mainRef = Constant.rootRef.child("Study");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line
                , getResources().getStringArray(R.array.category));

        preference.setAdapter(categoryAdapter);

        ArrayAdapter<String> studyAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line
                , studyArray);
        study.setAdapter(studyAdapter);


        ArrayAdapter<String> mm = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line
                , getResources().getStringArray(R.array.year));
        year.setAdapter(mm);



        Constant.userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nameField.setText(dataSnapshot.child("name").getValue().toString());
                emailField.setText(dataSnapshot.child("email").getValue().toString());
                phoneField.setText(dataSnapshot.child("phone").getValue().toString());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        study.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                studyName = studyArray[i];
                selectedStudyIn = i;
                if (i == 1) {
                    ArrayAdapter<String> yearAd = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line
                            , CollegeList);
                    college.setAdapter(yearAd);
                } else if (i == 2){
                    ArrayAdapter<String> yearAd = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line
                            , SchoolList);
                    college.setAdapter(yearAd);

                } else if (i == 3){
                    ArrayAdapter<String> yearAd = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line
                            , CoachingList);
                    college.setAdapter(yearAd);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        college.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (selectedStudyIn == 1) {
                    collegeName = CollegeList.get(i);
                }else if (selectedStudyIn == 2){
                    collegeName = SchoolList.get(i);
                }else if (selectedStudyIn == 3){
                    collegeName = CoachingList.get(i);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        course.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                courceName = CourseList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                yearName = getResources().getStringArray(R.array.year)[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        edittxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(nameField.getText().toString())){
                    nameField.setError("Empty");
                    nameField.requestFocus();
                }else if (TextUtils.isEmpty(emailField.getText().toString())){
                    emailField.setError("Empty");
                    emailField.requestFocus();
                }else if (TextUtils.isEmpty(phoneField.getText().toString())){
                    phoneField.setError("Empty");
                    phoneField.requestFocus();
                }else if (study.getSelectedItem().toString().equals("Select")){
                    Toast.makeText(UserProfileActivity.this, "select study in", Toast.LENGTH_SHORT).show();
                }
                else if (collegeName.equals("")){
                    Toast.makeText(UserProfileActivity.this, "select study from", Toast.LENGTH_SHORT).show();
                }else if (courceName.equals("")){
                    Toast.makeText(UserProfileActivity.this, "select course name", Toast.LENGTH_SHORT).show();

                }else {
                    HashMap hm = new HashMap();
                    hm.put("name",nameField.getText().toString());
                    hm.put("email",emailField.getText().toString());
                    hm.put("phone",phoneField.getText().toString());
                    hm.put("study_in",studyName);
                    hm.put("study_from",collegeName);
                    hm.put("course",courceName);
                    hm.put("year",yearName);

                    Constant.userRef.updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            Toast.makeText(UserProfileActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void loadData(){
        mainRef.child("College").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    CollegeList.add(ds.child("category").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mainRef.child("School").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    SchoolList.add(ds.child("category").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mainRef.child("Coaching").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    CoachingList.add(ds.child("category").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mainRef.child("Course").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    CourseList.add(ds.child("category").getValue().toString());
                }
                loadUserData();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void loadUserData() {

        Constant.userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayAdapter<String> pp = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line
                        , CourseList);
                course.setAdapter(pp);
                if (dataSnapshot.hasChild("study_in")){
                    studyName = dataSnapshot.child("study_in").getValue().toString();
                    collegeName = dataSnapshot.child("study_from").getValue().toString();
                    collegeNameFromServer = dataSnapshot.child("study_from").getValue().toString();
                    courceName = dataSnapshot.child("course").getValue().toString();
                    yearName = dataSnapshot.child("year").getValue().toString();
                    setSelection();
                }else {
                    collegeName = CollegeList.get(0);
                    courceName = CourseList.get(0);
                    yearName = getResources().getStringArray(R.array.year)[0];
                }
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void setSelection() {
        study.setSelection(getIndex(studyArray,studyName));

        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (selectedStudyIn==1){
                    college.setSelection(CollegeList.indexOf(collegeNameFromServer));
                }else if (selectedStudyIn==2){
                    college.setSelection(SchoolList.indexOf(collegeNameFromServer));
                }else if (selectedStudyIn==3){
                    college.setSelection(CoachingList.indexOf(collegeNameFromServer));
                }

            }
        }, 200);
        course.setSelection(CourseList.indexOf(courceName));

        year.setSelection(getIndex(getResources().getStringArray(R.array.year),yearName));
    }


    int getIndex(String[] test,String txt){
        List<String> str = Arrays.asList(test);
        return str.indexOf(txt);
    }

}
