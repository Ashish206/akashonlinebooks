package com.dits.akashonlinebooks.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.extra.CountDrawable;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {
    SearchView searchView;
    TextView heading;
    RecyclerView recyclerView;
    String category;
    ArrayList<BookDataModel> bookList;
    ImageView filterBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        bookList = new ArrayList<>();


        filterBtn = findViewById(R.id.filter_btn);
        filterBtn.setVisibility(View.GONE);
        searchView = findViewById(R.id.search_bar);
        heading = findViewById(R.id.titleText);
        heading.setText(category);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = findViewById(R.id.recycler_view);


        Constant.BooksDetailRef = FirebaseDatabase.getInstance().getReference().child("BooksDetail");
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);




        loadFirst("");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (s.equals("")){
                    loadFirst("");
                }else {
                    searchTest(s);
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.equals("")){
                    loadFirst("");
                }else {
                    searchTest(s);
                }
                return true;
            }
        });

    }

    void loadFirst(String sText){

        FirebaseRecyclerOptions<BookDataModel> options =
                new FirebaseRecyclerOptions.Builder<BookDataModel>()
                        .setQuery(Constant.BooksDetailRef.limitToFirst(10), BookDataModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter2 = new FirebaseRecyclerAdapter<BookDataModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_card_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(final ItemViewHolder holder, final int position, final BookDataModel model) {
                holder.setItemName(model.getBookName());
                holder.setItemImage(model.getImage(),getApplicationContext());
                holder.setItemCost(model.getMRP());
                holder.setItemActualCost(model.getActualCost());
                holder.setPersentageText(Float.parseFloat(model.getMRP()),Float.parseFloat(model.getActualCost()));
                final String key =getRef(position).getKey();

//                holder.mView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(getApplicationContext(), BookDetailActivity.class);
//                        intent.putExtra("key",key);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                                (Activity) getApplicationContext(), holder.getImageView(), "image_transition");
//                        startActivity(intent, options.toBundle());
//                    }
//                });

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();

            }
        };
        adapter2.startListening();
        recyclerView.setAdapter(adapter2);
    }

    void searchTest(String sText){

        FirebaseRecyclerOptions<BookDataModel> options =
                new FirebaseRecyclerOptions.Builder<BookDataModel>()
                        .setQuery(Constant.BooksDetailRef.orderByChild("BookName").startAt(sText).endAt(sText+"\uf8ff").limitToFirst(10), BookDataModel.class)
                        .build();

        FirebaseRecyclerAdapter adapter2 = new FirebaseRecyclerAdapter<BookDataModel, ItemViewHolder>(options) {
            @Override
            public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.single_card_layout, parent, false);

                return new ItemViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(final ItemViewHolder holder, final int position, final BookDataModel model) {
                holder.setItemName(model.getBookName());
                holder.setItemImage(model.getImage(),getApplicationContext());
                holder.setItemCost(model.getMRP());
                holder.setItemActualCost(model.getActualCost());
                holder.setPersentageText(Float.parseFloat(model.getMRP()),Float.parseFloat(model.getActualCost()));
                final String key =getRef(position).getKey();

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(TestActivity.this, model.getBookName(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();

            }
        };
        adapter2.startListening();
        recyclerView.setAdapter(adapter2);
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        BookDataModel item;
        View mView;


        public ItemViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }

        public void setData(BookDataModel item) {
            this.item = item;

        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.name_text);
            book_name.setText(item);
        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemImage(String item, Context context) {
            ImageView itemImage = mView.findViewById(R.id.product_image);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
        }

        public ImageView getImageView(){
            ImageView itemImage = mView.findViewById(R.id.product_image);
            return itemImage;
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.percentageTxt);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage+"%");
        }


    }
}
