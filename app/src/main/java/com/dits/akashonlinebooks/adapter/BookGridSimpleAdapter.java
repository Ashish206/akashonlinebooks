package com.dits.akashonlinebooks.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.BookDataModel;

import java.util.ArrayList;
import java.util.List;

public class BookGridSimpleAdapter extends RecyclerView.Adapter<BookGridSimpleAdapter.ViewHolder>  implements Filterable {

    protected ItemListener mListener;
    private Context context;
    private ArrayList<BookDataModel> bookDataList;
    private ArrayList<BookDataModel> filteredProductList;
    BookDataModel temp;

    public BookGridSimpleAdapter(Context context,  ItemListener itemListener){
        this.context = context;
        this.mListener = itemListener;
        bookDataList = new ArrayList<>();
        filteredProductList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.book_detail_grid_single_layout, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        temp = filteredProductList.get(i);
        holder.setItemName(temp.getBookName());
        holder.setItemImage(temp.getImage(),context);
        holder.setItemCost(temp.getMRP());
        holder.setItemActualCost(temp.getActualCost());
        holder.setPersentageText(Float.parseFloat(temp.getMRP()),Float.parseFloat(temp.getActualCost()));
        holder.setKey(temp.getBookKey());

    }


    @Override
    public int getItemCount() {
        return filteredProductList == null ? 0 : filteredProductList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void remove(BookDataModel r) {
        int position = bookDataList.indexOf(r);
        if (position > -1) {
            bookDataList.remove(position);
            filteredProductList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public BookDataModel getItem(int position) {
        return bookDataList.get(position);
    }

    public void add(BookDataModel r) {
        bookDataList.add(r);
        filteredProductList.add(r);
        notifyItemInserted(bookDataList.size() - 1);
    }

    public void addAll(List<BookDataModel> moveResults) {
        for (BookDataModel result : moveResults) {
            add(result);
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    filteredProductList = bookDataList;
                } else {
                    ArrayList filteredList = new ArrayList<>();

                    for (BookDataModel androidVersion : bookDataList) {
                        String row = androidVersion.getBookName()
                                +" "+androidVersion.getAuthor()
                                +" "+androidVersion.getCategory()
                                +" "+androidVersion.getPublisher();
                        if (row.toLowerCase().trim().contains(charString.toLowerCase().trim())) {
                            filteredList.add(androidVersion);
                        }
                    }

                    filteredProductList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredProductList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                filteredProductList = (ArrayList) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ItemListener {
        void onItemClick(String key, ImageView imageView);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        String key;
        View mView;
        ImageView itemImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
            itemView.setOnClickListener(this);
        }

        public void setKey(String key){
            this.key = key;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.name_text);
            book_name.setText(Constant.upperCaseWords(item));

        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemImage(String item, Context context) {
            itemImage = mView.findViewById(R.id.product_image);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).thumbnail(0.1f).into(itemImage);
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.percentageTxt);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(String.valueOf(100-percentage+"% off"));
        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(key,itemImage);
            }
        }

    }
}

