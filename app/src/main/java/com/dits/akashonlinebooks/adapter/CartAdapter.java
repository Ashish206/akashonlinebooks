package com.dits.akashonlinebooks.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.activity.BookDetailActivity;
import com.dits.akashonlinebooks.activity.CartActivity;
import com.dits.akashonlinebooks.activity.ChangeAddressActivity;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.google.firebase.auth.FirebaseAuth;
import com.mikhaellopez.hfrecyclerview.HFRecyclerView;

import java.util.ArrayList;
import java.util.List;

import static com.dits.akashonlinebooks.activity.CartActivity.proceedBtn;

public class CartAdapter extends HFRecyclerView<BookDataModel> {
    protected ItemListener mListener;
    private Context context;
    private ArrayList<BookDataModel> bookDataList;
    BookDataModel temp;
    RemoveCartItem removeCartItem;
    EditCartItem editCartItem;


    public CartAdapter(Context context, ArrayList values, ItemListener itemListener){
        super(true, true);
        this.context = context;
        this.bookDataList = values;
        this.mListener = itemListener;
        this.removeCartItem = (RemoveCartItem) itemListener;
        this.editCartItem = (EditCartItem) itemListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;
            temp = bookDataList.get(i-1);
            holder.setItemName(Constant.upperCaseWords(temp.getBookName()));
            holder.setItemImage(temp.getImage(),context);
            holder.setItemCost(temp.getMRP());
            holder.setItemActualCost(temp.getActualCost());
            holder.setPublisherName(temp.getPublisher());
            holder.setAuthorName(temp.getAuthor());
            holder.setPersentageText(Float.parseFloat(temp.getMRP()),Float.parseFloat(temp.getActualCost()));
            holder.setKey(temp.getBookKey());
            holder.setQuantity(temp.getQuantity());
            holder.setRemoveBtn(temp.getBookKey());
            holder.setEditBtn(temp.getBookKey(),temp.getBookQuantity());
            proceedBtn.setEnabled(true);


        } else if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
            holder.setNamePostalCode(Constant.userName+", "+Constant.postalCode);
            holder.setAddress(Constant.userAddress);
            holder.changeAddress();

        } else if (viewHolder instanceof FooterViewHolder) {

            FooterViewHolder holder = (FooterViewHolder) viewHolder;

            holder.setItemCount(Constant.CartItemCount);

            holder.setTotalPrice(String.valueOf((int)CartActivity.totalAmount));
            holder.setReferralAmount((int)CartActivity.totalAmount);
            holder.setTotalPayable(String.valueOf((int)CartActivity.totalAmount+Integer.valueOf(Constant.deliveryCharge)-Constant.useableReferralAmount));
            holder.setDeliveryCharge(Constant.deliveryCharge);
            holder.setSaveTxt(String.valueOf((int)CartActivity.totalActualAmount-CartActivity.totalAmount+Constant.useableReferralAmount));


        }

    }

    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.single_cart_item_list, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(R.layout.cart_header_layout, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return new FooterViewHolder(inflater.inflate(R.layout.cart_footer_layout, parent, false));
    }


    public interface ItemListener {
        void onItemClick(String key,ImageView imageView);
    }


    public interface RemoveCartItem{
        void onRemoveItemClick(String key);
    }

    public interface EditCartItem{
        void onEditCartItemListener(String key,Integer quantity);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        String key;
        View mView;
        ImageView itemImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
            itemView.setOnClickListener(this);
        }

        public void setKey(String key){
            this.key = key;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.book_name);
            book_name.setText(item);

        }

        public void setAuthorName(String item) {
            TextView item_name = mView.findViewById(R.id.author_name);
            item_name.setText(item);

        }

        public void setPublisherName(String item) {
            TextView item_name = mView.findViewById(R.id.publisher_name);
            item_name.setText(item);

        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setQuantity(int item) {
            TextView book_name = mView.findViewById(R.id.product_quantity);
            book_name.setText("Qty : "+item);
        }

        public void setItemImage(String item, Context context) {
            itemImage = mView.findViewById(R.id.book_image);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }

        public void setRemoveBtn(final String item){
            TextView remmoveBtn = mView.findViewById(R.id.delete_cart);

            remmoveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Constant.cartRef.child(item).setValue(null);

                    if (removeCartItem != null) {
                        removeCartItem.onRemoveItemClick(item);
                    }

                }
            });
        }
        public void setEditBtn(final String item, final Integer quantity){
            TextView edit = mView.findViewById(R.id.edit_cart);

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Constant.cartRef.child(item).setValue(null);

                    if (editCartItem != null) {
                        editCartItem.onEditCartItemListener(item,quantity);
                    }

                }
            });
        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(key,itemImage);
            }
        }

    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        View mView;
        HeaderViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
        public void setNamePostalCode(String item) {
            TextView item_name = mView.findViewById(R.id.name_postal_code);
            item_name.setText(item);
        }

        public void setAddress(String item) {
            TextView item_name = mView.findViewById(R.id.address);
            item_name.setText(item);
        }

        public void changeAddress() {
            TextView item_name = mView.findViewById(R.id.change_adr);
            item_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChangeAddressActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        View mView;
        FooterViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemCount(String item) {
            TextView item_name = mView.findViewById(R.id.total_item);
            item_name.setText("Price ("+item+" item)");
        }

        public void setTotalPrice(String item) {
            TextView item_name = mView.findViewById(R.id.total_price);
            item_name.setText("₹"+item);
        }

        public void setTotalPayable(String item) {
            TextView item_name = mView.findViewById(R.id.total_amount_payable);
            item_name.setText("₹"+item);
        }

        public void setDeliveryCharge(String item) {
            TextView item_name = mView.findViewById(R.id.delivery_charge);
            item_name.setText("₹"+item);
        }

        public void setReferralAmount(int item) {
            LinearLayout ll = mView.findViewById(R.id.referral_lay);
            TextView item_name = mView.findViewById(R.id.referral_amount);
            if (item!=0 && item>=100 && Constant.referralAmount>=10){
                if (Constant.referralAmount>=50){
                    Constant.useableReferralAmount = 50;
                }else {
                    Constant.useableReferralAmount = Constant.referralAmount;
                }
                item_name.setText("- ₹"+Constant.useableReferralAmount);
                ll.setVisibility(View.VISIBLE);
                item_name.setTextColor(context.getResources().getColor(R.color.red));
            }else {
                Constant.useableReferralAmount=0;
                ll.setVisibility(View.GONE);
                item_name.setText("₹"+0);
            }

        }

        public void setSaveTxt(String item) {
            TextView item_name = mView.findViewById(R.id.save_txt);
            item_name.setText("You will save total ₹"+item+" on this order");
        }

    }
}
