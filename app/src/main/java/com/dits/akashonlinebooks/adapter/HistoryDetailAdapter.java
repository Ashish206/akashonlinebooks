package com.dits.akashonlinebooks.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.activity.HistoryDetailActivity;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.HistoryDetailModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.mikhaellopez.hfrecyclerview.HFRecyclerView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class HistoryDetailAdapter extends HFRecyclerView<HistoryDetailModel> {
    protected CartAdapter.ItemListener mListener;
    private Context context;
    private ArrayList<HistoryDetailModel> bookDataList;
    HistoryDetailModel temp;
    CancelOrder cancelOrder;
    Rating rating;
    DownloadInvoice downloadInvoice;

    public HistoryDetailAdapter(Context context, ArrayList values,CancelOrder cancelOrder){
        super(true, true);
        this.context = context;
        this.bookDataList = values;
        this.cancelOrder = cancelOrder;
        this.rating = (Rating) cancelOrder;
        this.downloadInvoice = (DownloadInvoice) cancelOrder;

//        this.removeCartItem = (RemoveCartItem) itemListener;
//        this.editCartItem = (EditCartItem) itemListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) viewHolder;
            temp = bookDataList.get(i-1);
            holder.setItemName(Constant.upperCaseWords(temp.getBookName()));
            holder.setItemImage(temp.getImage(),context);
            holder.setItemCost(temp.getMRP());
            holder.setItemActualCost(temp.getActualCost());
            holder.setPublisherName(temp.getPublisher());
            holder.setAuthorName(temp.getAuthor());
            holder.setPersentageText(Float.parseFloat(temp.getMRP()),Float.parseFloat(temp.getActualCost()));
            holder.setKey(temp.getKey());
            holder.setQuantity(temp.getQuantity());
            holder.ratingBar(temp.getRated(),temp.getHistoryKey());
            holder.reviewBtn(temp.getReviewed(),temp.getHistoryKey());


        } else if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
            holder.setNamePostalCode(HistoryDetailActivity.userName +", "+HistoryDetailActivity.userPostalCode);
            holder.setAddress(HistoryDetailActivity.userAddress);
            holder.setDeliveryStatus(HistoryDetailActivity.delivered);



        } else if (viewHolder instanceof FooterViewHolder) {

            FooterViewHolder holder = (FooterViewHolder) viewHolder;

            holder.setItemCount(String.valueOf(HistoryDetailActivity.count));
            holder.setTotalPrice(String.valueOf(Integer.parseInt(HistoryDetailActivity.Cost)-Integer.parseInt(HistoryDetailActivity.deliveryCharge)+Integer.valueOf(HistoryDetailActivity.referralAmount)));
            holder.setTotalPayable(String.valueOf(HistoryDetailActivity.Cost));
            holder.setDeliveryCharge("₹"+HistoryDetailActivity.deliveryCharge);
            holder.setSaveTxt(String.valueOf(Integer.valueOf(HistoryDetailActivity.ActualCost)-Integer.parseInt(HistoryDetailActivity.Cost)+Integer.parseInt(HistoryDetailActivity.deliveryCharge)));
            holder.setReferralAmount(Integer.valueOf(HistoryDetailActivity.referralAmount));
            holder.cancelBtn(HistoryDetailActivity.delivered,HistoryDetailActivity.orderKey);

        }


    }

    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.history_detail_view_item_layout, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(R.layout.history_detail_view_header, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return new FooterViewHolder(inflater.inflate(R.layout.history_detail_view_footer, parent, false));
    }

    public interface CancelOrder{
        void onClacelOrderClick(String key);
    }

    public interface Review{
        void onReviewListener();
    }

    public interface Rating{
        void onRateingListener(String key,float rate,float oldRating,String historyKey);
    }

    public interface DownloadInvoice{
        void onDownloadInvoiceClick();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        String key;
        View mView;
        ImageView itemImage;
        int x = 0;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
            itemView.setOnClickListener(this);
        }

        public void setKey(String key){
            this.key = key;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.book_name);
            book_name.setText(item);

        }

        public void setAuthorName(String item) {
            TextView item_name = mView.findViewById(R.id.author_name);
            item_name.setText(item);

        }

        public void setPublisherName(String item) {
            TextView item_name = mView.findViewById(R.id.publisher_name);
            item_name.setText(item);

        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setQuantity(String item) {
            TextView book_name = mView.findViewById(R.id.product_quantity);
            book_name.setText("Qty : "+item);
        }

        public void setItemImage(String item, Context context) {
            itemImage = mView.findViewById(R.id.book_image);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(itemImage);
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.discount);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(100-percentage + "% off");
        }


        public void ratingBar(String rated, final String historyKey){
            MaterialRatingBar materialRatingBar = mView.findViewById(R.id.rating_bar);
            View v = mView.findViewById(R.id.viw);
            if (rated.equals("false") && HistoryDetailActivity.delivered.equals("Delivered")){
                materialRatingBar.setVisibility(View.VISIBLE);
                v.setVisibility(View.VISIBLE);
                materialRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, final float v, boolean b) {
                        x=0;
                        Constant.BooksDetailRef.child(key).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (v != 0 && x == 0){
                                    rating.onRateingListener(key,v,Float.parseFloat(dataSnapshot.child("Rating").getValue().toString()),historyKey);
                                    x++;
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }
                });
            }else {
                materialRatingBar.setVisibility(View.GONE);

            }



        }

        public void reviewBtn(String s, final String h){
            final View v = mView.findViewById(R.id.viw);
            final EditText review = itemView.findViewById(R.id.review);
            final TextView sendBtn = itemView.findViewById(R.id.sendReview);
            if (s.equals("false") && HistoryDetailActivity.delivered.equals("Delivered")){

                sendBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
                        final String date = dateFormat.format(Calendar.getInstance().getTime());

                        HashMap hm = new HashMap();
                        hm.put("name",Constant.userName);
                        hm.put("uid",Constant.uid);
                        hm.put("review",review.getText().toString());
                        hm.put("Date",date);
                        String xbxb = FirebaseDatabase.getInstance().getReference().child("Review").child(key).push().getKey();
                        FirebaseDatabase.getInstance().getReference().child("Review").child(key).child(xbxb).updateChildren(hm).addOnCompleteListener(new OnCompleteListener() {
                            @Override
                            public void onComplete(@NonNull Task task) {
                                if (task.isSuccessful()){

                                    Constant.historyRef.child(HistoryDetailActivity.orderref).child(h).child("reviewed").setValue("true").addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                Toast.makeText(context, "Submitted", Toast.LENGTH_SHORT).show();
                                                review.setVisibility(View.GONE);
                                                v.setVisibility(View.GONE);
                                                sendBtn.setVisibility(View.GONE);
                                            }
                                        }
                                    });

                                }
                            }
                        });
                    }
                });


            }else {
                review.setVisibility(View.GONE);
                v.setVisibility(View.GONE);
                sendBtn.setVisibility(View.GONE);
            }

        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(key,itemImage);
            }
        }

    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        View mView;
        HeaderViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
        public void setNamePostalCode(String item) {
            TextView item_name = mView.findViewById(R.id.name_postal_code);
            item_name.setText(item);
        }

        public void setAddress(String item) {
            TextView item_name = mView.findViewById(R.id.address);
            item_name.setText(item);
            item_name.setMaxLines(3);
        }

        public void setDeliveryStatus(String totalCost) {
            TextView book_name = mView.findViewById(R.id.delivery_status);
            book_name.setText(totalCost);

            if (totalCost.equals("Order Placed"))
            {
                book_name.setTextColor(context.getResources().getColor(R.color.yello));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_yello_dot, 0, 0, 0);
            }
            else if (totalCost.equals("Cancelled"))
            {
                book_name.setTextColor(context.getResources().getColor(R.color.red));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_red_dot, 0, 0, 0);
            }
            else {
                book_name.setTextColor(context.getResources().getColor(R.color.green));
                book_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_green_dot, 0, 0, 0);
            }

        }

    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        View mView;
        FooterViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setItemCount(String item) {
            TextView item_name = mView.findViewById(R.id.total_item);
            item_name.setText("Price ("+item+" item)");
        }

        public void setTotalPrice(String item) {
            TextView item_name = mView.findViewById(R.id.total_price);
            item_name.setText("₹"+item);
        }

        public void setTotalPayable(String item) {
            TextView item_name = mView.findViewById(R.id.total_amount_payable);
            item_name.setText("₹"+item);

        }

        public void setDeliveryCharge(String item) {
            TextView item_name = mView.findViewById(R.id.delivery_charge);
            item_name.setText(item);
        }

        public void setSaveTxt(String item) {
            TextView item_name = mView.findViewById(R.id.save_txt);
            item_name.setText("You saved total ₹"+item+" on this order");
        }

        public void setReferralAmount(int item) {
            LinearLayout ll = mView.findViewById(R.id.referral_lay);
            TextView item_name = mView.findViewById(R.id.referral_amount);
            if (item!=0){
                ll.setVisibility(View.VISIBLE);
                item_name.setText("- ₹"+item);
                item_name.setTextColor(context.getResources().getColor(R.color.red));
            }else {
                ll.setVisibility(View.GONE);
                item_name.setText("₹"+0);
            }

        }

        public void cancelBtn(String status, final String key){
            Button cancelBtn = mView.findViewById(R.id.cancel_btn);
            final Button downloadInvoiceBtn = mView.findViewById(R.id.invoice_download_btn);
            if (status.equals("Order Placed")){
                cancelBtn.setVisibility(View.VISIBLE);
            }else {
                cancelBtn.setVisibility(View.GONE);
            }

            if (status.equals("Delivered")){
                downloadInvoiceBtn.setVisibility(View.VISIBLE);
            }

            downloadInvoiceBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (downloadInvoice!=null){
                        downloadInvoice.onDownloadInvoiceClick();
                    }
                }
            });

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cancelOrder != null) {
                        cancelOrder.onClacelOrderClick(key);
                    }
                }
            });



        }

    }
}
