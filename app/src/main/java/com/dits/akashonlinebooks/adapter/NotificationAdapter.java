package com.dits.akashonlinebooks.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.model.NotificationModel;
import java.util.ArrayList;

public class NotificationAdapter  extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>  {

    ArrayList<Object> arrayList;
    OnDelete onDelete;

    public NotificationAdapter(ArrayList<Object> arrayList,OnDelete onDelete){
        this.arrayList = arrayList;
        this.onDelete = onDelete;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View viewItem = inflater.inflate(R.layout.single_notification_layout, viewGroup, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        NotificationModel nm = (NotificationModel) arrayList.get(i);
        viewHolder.setQuestion(nm.getTitle(),i);
        viewHolder.setAnswer(nm.getMessage());
        viewHolder.setDate(nm.getDate());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public interface OnDelete{
        void onDelete(int i);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View mView;


        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
        }

        public void setQuestion(String item,final int i) {
            TextView name = mView.findViewById(R.id.title);
            name.setText(item);
            ImageView iv = mView.findViewById(R.id.remove_btn);

            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onDelete!=null){
                        onDelete.onDelete(i);
                    }
                }
            });

        }

        public void setAnswer(String item) {
            TextView book_name = mView.findViewById(R.id.message);
            book_name.setText(item);
        }

        public void setDate(String ans) {
            TextView book_name = mView.findViewById(R.id.date);
            book_name.setText(ans);
        }


    }
}
