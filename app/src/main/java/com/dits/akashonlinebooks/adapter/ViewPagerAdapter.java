package com.dits.akashonlinebooks.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.activity.HomeActivity;
import com.dits.akashonlinebooks.activity.MoreBtnActivity;
import com.dits.akashonlinebooks.extra.Constant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.paytm.pgsdk.easypay.manager.PaytmAssist.getContext;


public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private Integer [] images = {R.drawable.slide1, R.drawable.slide2,R.drawable.slide3};
    ArrayList imageUrlList;
    ArrayList clickArrayList;
    String query = "";
    int x = 1;
    String headingTxt;

    public ViewPagerAdapter(Context context, ArrayList imageUrlList,ArrayList arrayList2,String heading) {
        this.context = context;
        this.imageUrlList = imageUrlList;
        clickArrayList = arrayList2;
        headingTxt = heading;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout, null);
        ImageView imageView = view.findViewById(R.id.imageView);
        Glide.with(context).load(imageUrlList.get(position)).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(imageView);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position == 0){
                    if (!clickArrayList.get(0).toString().equals("null")){
                        setRecyclerView(clickArrayList.get(0).toString());
                    }
                } else if(position == 1){
                    if (!clickArrayList.get(1).toString().equals("null")){
                        setRecyclerView(clickArrayList.get(1).toString());
                    }
                } else {
                    if (!clickArrayList.get(2).toString().equals("null")){
                        setRecyclerView(clickArrayList.get(2).toString());
                    }
                }

            }
        });

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    public void setRecyclerView(String item) {
        query="";
        x=1;
        Constant.rootRef.child("OfferZoneBooks").child(item).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    if (dataSnapshot.getChildrenCount() > x){
                        query +="'"+ds.child("id").getValue().toString()+"',";
                    }else {
                        query +="'"+ds.child("id").getValue().toString()+"'";
                    }
                    x++;
                }
                Intent i = new Intent(HomeActivity.homeContext, MoreBtnActivity.class);
                i.putExtra("category",""+query);
                i.putExtra("heading",headingTxt);
                HomeActivity.homeContext.startActivity(i);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}
