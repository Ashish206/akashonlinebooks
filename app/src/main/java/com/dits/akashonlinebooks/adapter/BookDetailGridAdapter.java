package com.dits.akashonlinebooks.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.activity.BookDetailActivity;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.utils.PaginationAdapterCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class BookDetailGridAdapter extends RecyclerView.Adapter<BookDetailGridAdapter.ViewHolder>  implements Filterable {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    protected ItemListener mListener;
    private Context context;
    private ArrayList<BookDataModel> filteredProductList;
    BookDataModel temp;
    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private PaginationAdapterCallback mCallback;
    private String errorMsg;
    ShowNoItemText showNoItemTextListener;

    public BookDetailGridAdapter(Context context, ItemListener itemListener,PaginationAdapterCallback cc){
        this.context = context;
        this.mListener = itemListener;
        this.mCallback =  cc;
        showNoItemTextListener = (ShowNoItemText) itemListener;
        filteredProductList = new ArrayList<>();
    }

    @Override
    public BookDetailGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BookDetailGridAdapter.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.book_detail_grid_single_layout, parent, false);
                viewHolder = new ViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (getItemViewType(position)) {

            case ITEM:
                temp = filteredProductList.get(position);
                holder.setItemName(Constant.upperCaseWords(temp.getBookName()));
                holder.setItemImage(temp.getImage(),context);
                holder.setItemCost(temp.getMRP());
                holder.setItemActualCost(temp.getActualCost());
                holder.setPersentageText(Float.parseFloat(temp.getMRP()),Float.parseFloat(temp.getActualCost()));
                holder.setKey(temp.getBookKey());

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return filteredProductList == null ? 0 : filteredProductList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return (position == filteredProductList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;

    }

    public void add(BookDataModel r) {
        filteredProductList.add(r);
        notifyItemInserted(filteredProductList.size() - 1);
    }

    public void addAll(List<BookDataModel> moveResults) {
        for (BookDataModel result : moveResults) {
            add(result);
        }
    }

    public void checkListCount(){
        if (showNoItemTextListener!=null){
            if (getItemCount()!=0){
                showNoItemTextListener.showNoItemText(false);
            }else {
                showNoItemTextListener.showNoItemText(true);
            }
        }
    }

    public void addList(ArrayList<BookDataModel> list){
        filteredProductList = list;
    }

    public void remove(BookDataModel r) {
        int position = filteredProductList.indexOf(r);
        if (position > -1) {
            filteredProductList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new BookDataModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = filteredProductList.size() - 1;
        BookDataModel result = getItem(position);

        if (result != null) {
            filteredProductList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public BookDataModel getItem(int position) {
        return filteredProductList.get(position);
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void sortHignToLow(){
        Collections.sort(filteredProductList, new Comparator<BookDataModel>() {
            @Override
            public int compare(BookDataModel lhs, BookDataModel rhs) {
                return Integer.valueOf(rhs.getMRP()).compareTo(Integer.valueOf(lhs.getMRP()));
            }
        });
        notifyDataSetChanged();
    }
    public void sortLowToHigh(){
        Collections.sort(filteredProductList, new Comparator<BookDataModel>() {
            @Override
            public int compare(BookDataModel lhs, BookDataModel rhs) {
                return Integer.valueOf(lhs.getMRP()).compareTo(Integer.valueOf(rhs.getMRP()));
            }
        });
        notifyDataSetChanged();
    }
    @Override
    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//
//                if (charString.isEmpty()) {
//                    filteredProductList = bookDataList;
//                } else {
//                    ArrayList filteredList = new ArrayList<>();
//
//                    for (BookDataModel androidVersion : bookDataList) {
//                        String row = androidVersion.getBookName()
//                                +" "+androidVersion.getAuthor()
//                                +" "+androidVersion.getCategory()
//                                +" "+androidVersion.getPublisher();
//                        if (row.toLowerCase().trim().contains(charString.toLowerCase().trim())) {
//                            filteredList.add(androidVersion);
//                        }
//                    }
//
//                    filteredProductList = filteredList;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = filteredProductList;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//
//                filteredProductList = (ArrayList) filterResults.values;
//                notifyDataSetChanged();
//            }
//        };
        return null;
    }
    public interface ShowNoItemText{
        void showNoItemText(boolean b);
    }

    public interface ItemListener {
        void onItemClick(String key,ImageView imageView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        String key;
        View mView;
        ImageView itemImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
            itemView.setOnClickListener(this);
        }

        public void setKey(String key){
            this.key = key;
        }

        public void setItemName(String item) {
            TextView book_name = mView.findViewById(R.id.name_text);
            book_name.setText(Constant.upperCaseWords(item));

        }

        public void setItemCost(String item) {
            TextView book_name = mView.findViewById(R.id.cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemActualCost(String item) {
            TextView book_name = mView.findViewById(R.id.actual_cost_text);
            book_name.setText("₹"+item);
        }

        public void setItemImage(String item, Context context) {
            itemImage = mView.findViewById(R.id.product_image);
            Glide.with(context).load(item).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).thumbnail(0.1f).into(itemImage);
        }

        public void setPersentageText(float numerator,float denominator) {
            TextView itemImage = mView.findViewById(R.id.percentageTxt);
            int percentage = (int)(numerator * 100.0 / denominator + 0.5);
            itemImage.setText(String.valueOf(100-percentage+"% off"));
        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(key,itemImage);
            }
        }

    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(filteredProductList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    protected class LoadingVH extends BookDetailGridAdapter.ViewHolder implements View.OnClickListener {
        private LinearLayout mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loading_layout);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }
}
