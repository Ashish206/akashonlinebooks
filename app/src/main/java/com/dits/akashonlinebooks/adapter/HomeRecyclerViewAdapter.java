package com.dits.akashonlinebooks.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.dits.akashonlinebooks.R;
import com.dits.akashonlinebooks.activity.BookDetailActivity;
import com.dits.akashonlinebooks.activity.HomeActivity;
import com.dits.akashonlinebooks.activity.MoreBtnActivity;
import com.dits.akashonlinebooks.extra.ApiClient;
import com.dits.akashonlinebooks.extra.ApiInterface;
import com.dits.akashonlinebooks.extra.BookData;
import com.dits.akashonlinebooks.extra.Constant;
import com.dits.akashonlinebooks.model.BookDataModel;
import com.dits.akashonlinebooks.model.HomeRVModel;
import com.dits.akashonlinebooks.model.OfferIdModer;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements HomeHorizontalRecyclerAdapter.ItemListener {
    ArrayList<HomeRVModel> itemList;
    Context context;
    Activity activity;
    MoreBtnListener moreBtnListener;
    BannerClick bannerClick;
    String query = "";


    public HomeRecyclerViewAdapter(ArrayList arrayList, Context ctx, FragmentActivity activity,MoreBtnListener cc,BannerClick c2){
        itemList = arrayList;
        context = ctx;
        this.activity =activity;
        moreBtnListener = cc;
        bannerClick =  c2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (i) {
            case 1:
                View v1 = inflater.inflate(R.layout.recycler_view, viewGroup, false);
                viewHolder = new HomeItemRecyclerView(v1);
                break;
            case 2:
                View v2 = inflater.inflate(R.layout.slider_layout, viewGroup, false);
                viewHolder = new ViewHolder2(v2);
                break;
            case 3:
                View v3 = inflater.inflate(R.layout.banner_layout, viewGroup, false);
                viewHolder = new ViewHolder3(v3);
                break;
            case 4:
                View v4 = inflater.inflate(R.layout.recycler_view, viewGroup, false);
                viewHolder = new HomeOfferItemRecyclerView(v4);
                break;
            default:
                View v = inflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
                viewHolder = new HomeItemRecyclerView(v);
                break;
            }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        switch (viewHolder.getItemViewType()) {
            case 1:
                HomeItemRecyclerView h = (HomeItemRecyclerView) viewHolder;
                h.setFilter(itemList.get(i).getFilter());
                h.setCategory(itemList.get(i).getCategory_name());
                h.setRecyclerView(itemList.get(i).getFilter());
                break;
            case 2:
                ViewHolder2 vh1 = (ViewHolder2) viewHolder;
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                arrayList.add(itemList.get(i).getSlider1());
                arrayList.add(itemList.get(i).getSlider2());
                arrayList.add(itemList.get(i).getSlider3());
                arrayList2.add(itemList.get(i).getSlider1_click());
                arrayList2.add(itemList.get(i).getSlider2_click());
                arrayList2.add(itemList.get(i).getSlider3_click());
                vh1.setViewPager(arrayList,arrayList2,itemList.get(i).getCategory_name());
                break;
            case 3:
                ViewHolder3 vh3 = (ViewHolder3) viewHolder;
                vh3.setCategory(itemList.get(i).getCategory_name());
                vh3.setImageView(itemList.get(i).getImage());
                vh3.setRecyclerView(itemList.get(i).getBanner_click());
                break;
            case 4:
                HomeOfferItemRecyclerView h2 = (HomeOfferItemRecyclerView) viewHolder;
                h2.setCategory(itemList.get(i).getCategory_name());
                h2.setRecyclerView(itemList.get(i).getFilter());
                break;
            default:

                break;
        }

    }

    @Override
    public int getItemViewType(int position) {

        if (itemList.get(position).getType().equals("horizontal_list")) {
            return 1;
        } else if (itemList.get(position).getType().equals("slider")) {
            return 2;
        }else if (itemList.get(position).getType().equals("banner")) {
            return 3;
        }else if (itemList.get(position).getType().equals("offer_zone")) {
            return 4;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public interface MoreBtnListener {
        void onMoreBtnClick(String filterBy,String heading);
    }

    public interface BannerClick {
        void onBannerClick(String filterBy,String heading);
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {
        View view;

        Timer timer;
        final long DELAY_MS = 500;
        final long PERIOD_MS = 3000;
        ViewPager viewPager;
        public ViewHolder2(View v) {
            super(v);
            view = v;
        }
        public void setViewPager(ArrayList arrayList,ArrayList arrayList2,String heading) {

            viewPager = view.findViewById(R.id.viewPagerHome);

            ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(context,arrayList,arrayList2,heading);
            viewPager.setAdapter(viewPagerAdapter);
            final int[] currentPage = {0,1,2};
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                int i=0;
                public void run() {
                    if (i == 3) {
                        i= 0;
                    }
                    viewPager.setCurrentItem(currentPage[i] ,true);
                    i++;

                }
            };

            timer = new Timer(); // This will create a new Thread
            timer.schedule(new TimerTask() { // task to be scheduled
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, DELAY_MS, PERIOD_MS);
        }
    }

    public class ViewHolder3 extends RecyclerView.ViewHolder implements View.OnClickListener {

        View view;
        String filterBy;
        String headingTxt;
        int x=1;
        public ViewHolder3(View v) {
            super(v);
            view = v;
            view.setOnClickListener(this);
        }

        public void setCategory(String item) {
            headingTxt = item;
        }

        public void setImageView(String url) {
            ImageView iv = view.findViewById(R.id.banner_image);
            iv.setVisibility(View.VISIBLE);
            Glide.with(context).load(url).apply(new RequestOptions().placeholder(R.drawable.no_image).error(R.drawable.no_image)).into(iv);
        }

        public void setRecyclerView(String item) {
            filterBy="";
            query="";
            x=1;

            Constant.rootRef.child("OfferZoneBooks").child(item).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    filterBy="";
                    query="";
                    x=1;
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        if (dataSnapshot.getChildrenCount() > x){
                            query +="'"+ds.child("id").getValue().toString()+"',";
                        }else {
                            query +="'"+ds.child("id").getValue().toString()+"'";
                        }
                        x++;
                    }
                    filterBy = query;

                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        @Override
        public void onClick(View view) {
            if (bannerClick != null && filterBy!=""){
                bannerClick.onBannerClick(filterBy,headingTxt);
            }
        }
    }

    public class HomeItemRecyclerView extends RecyclerView.ViewHolder  implements View.OnClickListener {
        View mView;
        String filterBy;
        TextView moreBtnTxt;


        public HomeItemRecyclerView(View itemView) {
            super(itemView);
            mView=itemView;
            moreBtnTxt = mView.findViewById(R.id.moreBtn);
            moreBtnTxt.setOnClickListener(this);
        }

        public void setFilter(String filter){
            filterBy = filter;
        }


        public void setCategory(String item) {
            TextView catname = mView.findViewById(R.id.category_name);
            catname.setText(Constant.upperCaseWords(item));
        }

        public void setRecyclerView(String item) {
            final ArrayList<BookDataModel> bookDetailList = new ArrayList<>();
            final HomeHorizontalRecyclerAdapter bookDetailGridAdapter1 = new HomeHorizontalRecyclerAdapter(context, bookDetailList,HomeRecyclerViewAdapter.this);

            final ShimmerRecyclerView catname = mView.findViewById(R.id.recycler_view);
            //catname.showShimmerAdapter();
            catname.setHasFixedSize(true);
            catname.setNestedScrollingEnabled(false);
//            catname.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            //catname.setAdapter(bookDetailGridAdapter1);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<List<BookDataModel>> call = apiService.getHomeBookDetails(item);
            call.enqueue(new Callback<List<BookDataModel>>() {
                @Override
                public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                    for (BookDataModel result : response.body()) {
                        bookDetailList.add(result);
                    }
                    bookDetailGridAdapter1.notifyDataSetChanged();
                    catname.hideShimmerAdapter();
                    catname.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    catname.setAdapter(bookDetailGridAdapter1);
//                  HomeActivity.splash_screen.setVisibility(View.GONE);
//                  Handler mHandler = new Handler();
//                  mHandler.postDelayed(new Runnable() {
//                     @Override
//                     public void run() {
//                          //start your activity here
//                          HomeActivity.splash_screen.setVisibility(View.GONE);
//                      }
//                }, 1000);

                }

                @Override
                public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

                }
            });
        }

        @Override
        public void onClick(View view) {
            if (moreBtnListener != null) {
                moreBtnListener.onMoreBtnClick(filterBy,filterBy);
            }
        }
    }

    public class HomeOfferItemRecyclerView extends RecyclerView.ViewHolder  implements View.OnClickListener {
        View mView;
        String filterBy;
        TextView moreBtnTxt;
        String heading;
        int x = 1;

        public HomeOfferItemRecyclerView(View itemView) {
            super(itemView);
            mView=itemView;
            moreBtnTxt = mView.findViewById(R.id.moreBtn);
            moreBtnTxt.setOnClickListener(this);
        }

        public void setCategory(String item) {
            heading = item;
            TextView catname = mView.findViewById(R.id.category_name);
            catname.setText(item);
        }

        public void setRecyclerView(String item) {
            query="";
            x=1;
            Constant.rootRef.child("OfferZoneBooks").child(item).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    x=1;
                    filterBy = "";
                    query="";
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        if (dataSnapshot.getChildrenCount() > x){
                            query +="'"+ds.child("id").getValue().toString()+"',";
                            x++;
                        }else {
                            query +="'"+ds.child("id").getValue().toString()+"'";
                            x++;
                        }
                    }
                    filterBy = ""+query;
                    seupData(query);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        private void seupData(String query) {
            final ArrayList<BookDataModel> bookDetailList = new ArrayList<>();
            final HomeHorizontalRecyclerAdapter bookDetailGridAdapter1 = new HomeHorizontalRecyclerAdapter(context, bookDetailList,HomeRecyclerViewAdapter.this);

            final ShimmerRecyclerView catname = mView.findViewById(R.id.recycler_view);
            //catname.showShimmerAdapter();
            catname.setHasFixedSize(true);
            catname.setNestedScrollingEnabled(false);
            //catname.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            //
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<List<BookDataModel>> call = apiService.getOfferList(query);
            call.enqueue(new Callback<List<BookDataModel>>() {
                @Override
                public void onResponse(Call<List<BookDataModel>> call, Response<List<BookDataModel>> response) {
                    for (BookDataModel result : response.body()) {
                        bookDetailList.add(result);
                    }
                    bookDetailGridAdapter1.notifyDataSetChanged();
                    catname.hideShimmerAdapter();
                    catname.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    catname.setAdapter(bookDetailGridAdapter1);
                }

                @Override
                public void onFailure(Call<List<BookDataModel>> call, Throwable t) {

                }
            });
        }

        @Override
        public void onClick(View view) {
            if (moreBtnListener != null) {
                moreBtnListener.onMoreBtnClick(filterBy,heading);
            }
        }
    }

    @Override
    public void onItemClick(String key, ImageView imageView) {
        Intent intent = new Intent(context, BookDetailActivity.class);
        intent.putExtra("key",key);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                activity, imageView, "image_transition");
        context.startActivity(intent);
    }
}
